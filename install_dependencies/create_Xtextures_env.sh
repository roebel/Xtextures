#! /bin/bash

set -e
set -x
shopt -s failglob

mini_conda_path=

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

function usage() {
    echo "usage: $0 [-c|--conda_path ] [-h|--help]  environment_spec.yaml  ..."
    echo "install environment if it does not exist already"
    echo  "  -c path to conda (def assume conda is in PATH)."
    echo  "  -m force allow memory growth"
}

name=Xtextures
kname=$(uname -s)
if [ "$kname" = "Darwin" ]; then
    device=cpu
else
    device=gpu
fi
force_memory_growth=0
conda_path=$CONDA_EXE
while [ "$1" != "" ]; do
    case "$1" in
	-h | --help )
            shift
            usage
            exit 0
            ;;
	--conda_path | --conda )
            shift
            conda_path="$1"
            shift
            ;;
	-c | --cpu )
            device="cpu"
            shift
            ;;
        -b | --base )
            name=base
            update=1
            shift
            ;;
        -m | --mem_grow)
            shift
            force_memory_growth=1
            ;;
        -nmg | --no_mem_grow)
            shift
            force_memory_growth=0
            ;;
    esac
done


if [ "$conda_path" = "" ]; then
    echo no conda script known or given. Please specify the conda script to be used with the --conda command line
fi

if [ "$name" = "Xtextures" ]; then
    CONDA_ENV_BASE=$(dirname $(dirname $conda_path))/envs/$name
else
    CONDA_ENV_BASE=$(dirname $(dirname $conda_path))
fi

if [ "$kname" = "Darwin" ]; then
    macosx_vers=$(sw_vers | grep ProductVersion| cut  -f2 | cut -d. -f-2)
    if [ "$macosx_vers" = "10.11" ]; then
	$conda_path env update -n $name -f "${DIR}/tensorflow2.0-${device}-${kname}.yaml"
    else
	$conda_path env update -n $name -f "${DIR}/tensorflow2.2-${device}-${kname}.yaml"
    fi
else
    $conda_path env update -n $name -f "${DIR}/tensorflow2.2-${device}-${kname}.yaml"
fi

if [ "$force_memory_growth" = "1" ]; then
    mkdir -p  $CONDA_ENV_BASE/etc/conda/activate.d/
    mkdir -p  $CONDA_ENV_BASE/etc/conda/deactivate.d/
    cat >>  $CONDA_ENV_BASE/etc/conda/activate.d/allow_memory_growth.sh <<EOF
#! /bin/sh
export _prev_TF_FORCE_GPU_ALLOW_GROWTH=\$TF_FORCE_GPU_ALLOW_GROWTH
export TF_FORCE_GPU_ALLOW_GROWTH=true
EOF

    cat >>  $CONDA_ENV_BASE/etc/conda/deactivate.d/allow_memory_growth.sh <<EOF
#! /bin/sh
export TF_FORCE_GPU_ALLOW_GROWTH=\$_prev_TF_FORCE_GPU_ALLOW_GROWTH
EOF
 
fi


