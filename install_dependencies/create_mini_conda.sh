#! /bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

set -e
if [ $(uname) = "Darwin" ]; then
    if [ ! -f  Miniconda3-py37_4.9.2-MacOSX-x86_64.sh ] ; then
	curl https://repo.anaconda.com/miniconda/Miniconda3-py37_4.9.2-MacOSX-x86_64.sh -O 
    fi
    bash Miniconda3-py37_4.9.2-MacOSX-x86_64.sh -b -p $HOME/Anaconda3 -s

else
    if [ ! -f  Miniconda3-py37_4.9.2-Linux-x86_64.sh ] ; then
	wget https://repo.anaconda.com/miniconda/Miniconda3-py37_4.9.2-Linux-x86_64.sh
    fi
    bash Miniconda3-py37_4.9.2-Linux-x86_64.sh -b -p $HOME/Anaconda3 -s
fi

answer=
while true; do

  read -p "Do you want this Anaconda installation to be used as your default python package? (answer y/n): " answer

  if [ ${answer} != "y" -a ${answer} != "n" ]; then
    echo please answer only either y or n
  else
    break
  fi
done

if [ "${answer}" = "y" ];then
   $HOME/Anaconda3/bin/conda init
else
   printf "\\n"
   printf "You have chosen to not have conda modify your shell scripts at all.\\n"
   printf "To activate conda's base environment in your current shell session:\\n"
   printf "\\n"
   printf "eval \"\$($PREFIX/bin/conda shell.YOUR_SHELL_NAME hook)\" \\n"
   printf "\\n"
   printf "To install conda's shell functions for easier access, first activate, then:\\n"
   printf "\\n"
   printf "conda init\\n"
   printf "\\n"

   printf "alternatively make sure to run the Xtextures python scripts with $HOME/Anaconda3/bin/python"
fi

echo before using the Xtexture you need to create an Xtextures Anaconda environment containing the dependencies.
echo The easiest way may be to simply run:
echo
echo    $DIR/create_Xtextures_env.sh --conda $HOME/Anaconda3/bin/conda
echo
echo script hat will an Xtextures environment in your new conda environment
