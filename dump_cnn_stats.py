#! /usr/bin/env python

import os
# silence verbose TF feedback
if 'TF_CPP_MIN_LOG_LEVEL' not in os.environ:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"

import sys
placement_dir=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if placement_dir not in sys.path:
    sys.path.insert(0, placement_dir)

if __package__ is None:
    __package__ = os.path.abspath(os.path.dirname(__file__)).split("/")[-1]

import yaml
import json
from argparse import ArgumentParser
import tensorflow as tf
from .XTextures_impl.features import dump_cnn_stats
from .XTextures_impl.util import lockGPU, XtexturesLoader
from .as_pysrc.utils import create_logger as cl
from .as_pysrc.utils.reconstruct_command_line import reconstruct_command_line
from .XTextures_impl.XTextures_version import _version as sdtext_version


dump_stats_version = (1, 0, 0)

if __name__ == "__main__" :

    parser = ArgumentParser(description='dump CNN stats for given model')
    parser.add_argument('model_dir', help='model directory containing model specs, weights, and preprocessing params (Def: %(default)s)')
    parser.add_argument('-t', '--target_files', default = [], type=str, nargs="+",
                        help='filenames of all files to process for extracting target stats')
    parser.add_argument("--target_stats_dir", default=None,
                        help="Directory for storing target stats. If not provided it will be stored under stats subdir in"
                             "model_dir directory.")
    parser.add_argument('-v', '--verbose', action='store_true', help='display verbose output (Def: %(default)s)')

    args = parser.parse_args()
    command = reconstruct_command_line()

    default_log_format = "%(asctime)s::%(levelname)s::%(message)s"
    synth_log_format = "%(asctime)s::dump_stats::{}::%(levelname)s::%(message)s"
    ilog = cl.create_logger("sdtext", log_format=default_log_format)
    if args.verbose:
        cl.set_stream_level(ilog, cl.logging.INFO)
    else:
        cl.set_stream_level(ilog, cl.logging.INFO, stream_select="file")

    cl.add_file_logger(ilog, filename=os.path.join(args.model_dir, "command.log"),
                       log_level=cl.logging.INFO, file_mode="w")
    cl.set_stream_message_format(ilog, log_format=synth_log_format.format(args.model_dir))

    ilog.info("create_model with dump_stats version {} Xtextures implementation {}".format(dump_stats_version, sdtext_version))
    ilog.info(command)
    # we don't need a GPU, set all GPUs to invisible
    used_device = lockGPU(False)
    ilog.info(used_device)

    if os.path.exists(os.path.join(args.model_dir, "sdts_config.yaml")):
        with open(os.path.join(args.model_dir, "sdts_config.yaml"), "r") as fi:
            preproc_dict = yaml.load(stream=fi, Loader=XtexturesLoader)
    elif os.path.exists(os.path.join(args.model_dir, "sdts_config.json")):
        with open(os.path.join(args.model_dir, "sdts_config.json"), "r") as fi:
            preproc_dict = json.load(fp=fi)
    else:
        raise RuntimeError(f"none of the supported preprocessing config files: sdts_config.yaml and sdts_config.json, found in {args.model_dir}!")

    target_stats_dir = args.target_stats_dir
    if not target_stats_dir:
        target_stats_dir =  os.path.join(args.model_dir, "stats")

    with tf.device("cpu"):
        dump_cnn_stats(args.model_dir, preproc_dict, logger=ilog, target_files=args.target_files,
                          dump_target_stats_dir=target_stats_dir)

        with open(os.path.join(target_stats_dir, 'sdts_config.yaml'), "w") as fo:
            yaml.dump(preproc_dict, stream=fo, default_flow_style=False, indent=4)
        ilog.info('done.')


