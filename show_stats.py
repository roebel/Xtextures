#! /usr/bin/env python

import os
import sys

placement_dir=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if placement_dir not in sys.path:
    sys.path.insert(0, placement_dir)

if __package__ is None:
    __package__ = os.path.abspath(os.path.dirname(__file__)).split("/")[-1]

import numpy as np
from .as_pysrc.fileio import iovar as iov

import matplotlib
from matplotlib import pyplot as plt
from argparse import ArgumentParser

def flip_row_and_col(mat, ia,ib):
    row_ia=np.array(mat[ia])
    mat[ia] = mat[ib]
    mat[ib] = row_ia
    col_ia=np.array(mat[:,ia])
    mat[:,ia] = mat[:,ib]
    mat[:, ib] = col_ia
    return


if __name__ == "__main__" :

    parser = ArgumentParser(description='run texture synthesis using RI spectrograms')

    parser.add_argument('statsfiles',  nargs="+", help='the stats file to use')
    parser.add_argument("--bin_slice", default="0::25", help="a splice  that defines the bins to be used for the plots (Def: %(default)s)")
    parser.add_argument("--cmap", default="viridis", help="the matplotlib colormap to be used, note that appending _r to the colormap can be used to onvert it (Def: %(default)s)")

    parser.add_argument("--outfmt", default="pdf", help="image file format to ")
    parser.add_argument("--outdir", default=None, help="output directory, igf not given images are plotted")
    parser.add_argument("--figsize", default=(10,10), nargs=2, type=float, help="image size in inch (Def: %(default)s)")
    parser.add_argument("--dpi", default=80,  type=int, help="dots per inch for saved plots (Def: %(default)s)")
    parser.add_argument("-fs", "--fontsize", default=12,  type=int, help="fontsize  (Def: %(default)s)")

    parser.add_argument('-s','--sorted', action='store_true', help='sort diagonal according to activation (Def: %(default)s)')
    parser.add_argument('-sl','--show_tick_labels', action='store_true', help='if set x and y axis tics and tic labels are shown (Def: %(default)s)')
    parser.add_argument('-st','--show_title', action='store_true', help='if set the stats name is used as title of the image (Def: %(default)s)')
    parser.add_argument('-nf','--no_frame', action='store_true', help='if set yhe black frame aroun the image is removed (Def: %(default)s)')
    parser.add_argument('-v','--verbose', action='store_true', help='show progress over bins  and files (Def: %(default)s)')

    args = parser.parse_args()
    matplotlib.rcParams.update({'font.size': args.fontsize})
    if args.outdir:
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
        fig = plt.figure(figsize=args.figsize, constrained_layout=not args.no_frame, dpi=args.dpi)
        if args.no_frame:
            ax=plt.Axes(fig, [0.,0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)

    slice_spec_list = args.bin_slice.split(":")
    start_ind = 0
    stop_ind = None
    step = 1

    if len(slice_spec_list) == 1:
        start_ind = int(slice_spec_list[0])
        stop_ind = int(slice_spec_list[0]) + 1
    elif len(slice_spec_list) == 2:
        start_ind = int(slice_spec_list[0]) if slice_spec_list[0] else 0
        stop_ind = int(slice_spec_list[1]) if slice_spec_list[1] else None
    else:
        start_ind = int(slice_spec_list[0]) if slice_spec_list[0] else 0
        stop_ind = int(slice_spec_list[1]) if slice_spec_list[1] else None
        step = int(slice_spec_list[2]) if slice_spec_list[2] else 1
    slice_spec = slice(start_ind, stop_ind, step)
    if not args.outdir:
        fig = plt.figure(figsize=args.figsize, constrained_layout=True, dpi=args.dpi)
    for stfile in args.statsfiles:
        if args.verbose:
            print(f"processing {stfile}:", end ="")
        rr = iov.load_var(stfile)

        if stop_ind is None:
            end_ind =  rr["stat"].shape[0]
        else:
            end_ind = stop_ind


        for ii in range(start_ind, end_ind, step):
            if args.verbose:
                print(f" {ii},", end ="", flush=True)

            ss=rr["stat"][ii]
            if args.sorted:

                done = 0
                while done < ss.shape[0]-1:
                    dd = np.diag(ss)
                    ir = np.argmax(dd[done:])
                    flip_row_and_col(ss, done, ir+done)
                    done += 1

            if args.no_frame:
                ax.imshow(ss, cmap=args.cmap, origin='lower', aspect="auto")
            else:
                plt.imshow(ss, cmap=args.cmap, origin='lower', aspect="auto")

            if not args.no_frame:
                plt.title(".".join(os.path.basename(stfile).split(".")[:-1])+f"_{ii}")

                if not args.show_tick_labels:
                    plt.xticks([])
                    plt.yticks([])

            if args.outdir:
                if args.no_frame and False:
                    plt.savefig(os.path.join(args.outdir, ".".join(os.path.basename(stfile).split(".")[:-1])+f"_{ii}.{args.outfmt}"),
                                bbox_inches='tight', pad_inches = 0,
                                dpi=args.dpi)

                else:
                    plt.savefig(os.path.join(args.outdir, ".".join(os.path.basename(stfile).split(".")[:-1])+f"_{ii}.{args.outfmt}"),
                            dpi=args.dpi)
                if not args.no_frame:
                    plt.clf()
                    # ax=plt.Axes(fig, [0.,0., 1., 1.])
                    # ax.set_axis_off()
                    # fig.add_axes(ax)
                    
            else:
                plt.show()
        if args.verbose:
            print(flush=True)
