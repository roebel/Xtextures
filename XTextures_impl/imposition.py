from as_pysrc.utils import nextpow2_val
import numpy as np
import scipy.signal as sig
import time
import tensorflow as tf
import tensorflow.keras as keras
import numbers

from ..as_pysrc.fileio import iovar as iov
from .util import dinkus, write_snd
from .model import get_output_shapes_list, load_model
from .features import calc_GRAM, TFPreprocessor


class ModelLoss(tf.Module):
    def __init__(self, signal, model, tf_preprocessor, weights,
                 G_targ_list, targ_shapes, norm_mode,
                 logging_callback=None, progress_step = 5,
                 outfile_pat = None,
                 outfile_red = 0.7,
                 outfile_steps = 0,
                 commandline = None,
                 logger=None):
        super().__init__()
        self.signal = signal
        self.model = model
        self.tf_preprocessor = tf_preprocessor
        self.weights= tf.convert_to_tensor(weights)
        self.targ_shapes = targ_shapes
        net_inp = tf_preprocessor(self.signal)
        self.syn_shapes = get_output_shapes_list(self.model, net_inp)
        self.norm_mode = norm_mode
        self.layer_names = [oo.name for oo in self.model.outputs]
        self.loss_list = ([tf.Variable(0, dtype=tf.float32, shape=(), name="loss", trainable=False)]
                          + [tf.Variable(0, dtype=tf.float32, shape=(), name="GL{0:>02d}".format(il), trainable=False)
                             for il, _ in enumerate(self.model.outputs)])

        self.logging_callback = logging_callback
        self.iteration = tf.Variable(0, dtype=tf.int32, shape=(), name="it")
        self.progress_step = progress_step
        self.logger = logger
        self.conv_log = {}
        self.last_outfile_loss = None
        self.last_outfile_step = 0
        self.num_outputs = len(self.layer_names)
        self.ta_G_targ_normed = None
        self.ta_gram_synth_norm = None
        self.loss_norm=None
        self.outfile_pat = outfile_pat
        self.outfile_red = outfile_red
        self.outfile_steps = outfile_steps
        self.commandline = commandline
        self._prepare_gram_norm(G_targ_list=G_targ_list)

    def set_logging_callback(self, logging_callback):
        self.logging_callback = logging_callback

    def get_loss(self):
        return self.loss_list[0].numpy()

    @tf.function
    def value_and_grad_function(self):
        self.iteration.assign_add(1)
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            tape.watch(self.signal)
            loss = self.calc_loss()
        grad = tape.gradient(loss, self.signal)
        if self.logging_callback is not None:
            self.logging_callback()
        return loss, grad

    @tf.function
    def value_and_grad_function_for_signal(self, signal):
        self.signal.assign(signal)
        return self.value_and_grad_function()

    def do_log(self, force=False):
        it = self.iteration.numpy()
        for value in self.loss_list:
            name, valn  = value.name, value.numpy()
            if name not in self.conv_log:
                self.conv_log[name] = []
            self.conv_log[name].append((it, valn))
            tf.summary.scalar(name=name, data=valn, step=it)

        if self.logger is not None:
            if force or (self.progress_step==0 or (it % self.progress_step) == 0):
                logstr = [ "ep {0:4d}:".format(it)]
                for value in  self.loss_list:
                    logstr.append( "{0:4s} {1:.5g}".format(value.name[:4], value.numpy()))
                self.logger.info(logstr[0]+', '.join(logstr[1:]))

        if self.outfile_pat is not None:
            np_loss = self.loss_list[0].numpy()
            if self.last_outfile_loss is None:
                self.last_outfile_loss = np_loss
                self.last_outfile_step = it
            elif ((self.last_outfile_loss * self.outfile_red  > np_loss)
                  or  (self.outfile_steps and (self.last_outfile_step + self.outfile_steps < it))):
                outfile= self.outfile_pat.format(it, np_loss)
                self.last_outfile_loss = np_loss
                self.last_outfile_step = it
                if self.logger is not None:
                    self.logger.info(f"writing intermediate file: {outfile}")
                write_snd(outfile,
                          self.signal.numpy(), samp_rate=self.tf_preprocessor.srate, file_format="wav",
                          enc="float32", comments=self.commandline, logger=self.logger)

    def _prepare_gram_norm(self, G_targ_list):
        self.ta_G_targ_normed = tf.TensorArray(size=len(self.targ_shapes), dtype=tf.float32, infer_shape=False,
                                               clear_after_read=False)
        self.ta_gram_synth_norm = tf.TensorArray(size=len(self.targ_shapes), dtype=tf.float32, infer_shape=True,
                                                 clear_after_read=False, element_shape=())
        self.loss_norm = tf.Variable(0, dtype=tf.float32, shape=())
        for il, (G_targ, targ_shape, syn_shape) in enumerate(zip(G_targ_list, self.targ_shapes, self.syn_shapes)):
            if self.norm_mode == 'gatys':
                gram_targ_norm = np.prod(targ_shape[1:])
                gram_synth_norm = np.prod(syn_shape[1:])
            elif self.norm_mode == 'usty':
                temp = np.sqrt(np.sum(np.square(G_targ)))
                gram_targ_norm = temp
                gram_synth_norm = temp
            elif self.norm_mode == 'nusty':
                temp = np.sqrt(np.sum(np.square(G_targ)))
                gram_targ_norm= temp
                gram_synth_norm = (temp * syn_shape[2] / targ_shape[2])
            elif self.norm_mode is None:
                gram_targ_norm = 1.
                gram_synth_norm = 1.
            else:
                raise RuntimeError('synthTwinGram::error: {} is not a valid normalization'.format(self.norm_mode))
            self.ta_G_targ_normed = self.ta_G_targ_normed.write(il, G_targ / gram_targ_norm)
            self.ta_gram_synth_norm = self.ta_gram_synth_norm.write(il, gram_synth_norm)
            self.loss_norm.assign_add(self.weights[il] * np.sqrt(np.sum(np.square(G_targ))) / gram_targ_norm)


    def calc_loss(self):
        net_inp = self.tf_preprocessor(self.signal)
        G_synt_arr = calc_GRAM(ri_spec=net_inp, model=self.model, name="GSYN")
        loss_no_normed = tf.constant(0, dtype=tf.float32)
        for il in range(self.num_outputs):
            G_synt_normed = G_synt_arr.read(il)/ self.ta_gram_synth_norm.read(il)
            G_targ_normed = self.ta_G_targ_normed.read(il)
            err = tf.sqrt(tf.math.reduce_sum(tf.square((G_targ_normed - G_synt_normed))))
            self.loss_list[il+1].assign(err/self.loss_norm)
            loss_no_normed = loss_no_normed + self.weights[il] * err
        ret_val= loss_no_normed / self.loss_norm
        self.loss_list[0].assign(ret_val)
        return ret_val



def impose_cnn_stats(model_dir, target_stats_files, norm='usty', preproc_dict=None, weights=None,
                  init_data=None, samp_rate=None, exq_data=None,
                  num_iter=1000, log_dir=None, gather_run_metadata=False, syn_sig_len=None,
                  progress=5, logger=None, opt="l-bfgs-b",
                  adam_learning_rate=0.001,  outfile_pat=None, outfile_red=0.7, outfile_steps=0, commandline=None):
    """
    Synthesize sound texture signal from existing target texture using CNN-based parametric synthesis and RI
    representation: a base time signal is optimized until its parameter tensors draw close to those of the target
    texture.

    Parameters
    ----------
    target : Union[array, None]
        Target time signal. If given as None (or not given) the parameters target_stats_files, and init_data or syn_sig_len
        have to be provided

    model : keras model
        Model of the CNN used for synthesis. Convention must be chhanels-last ("Height-Width-Depth").

    norm : string
        Choice between 'gatys' and 'usty' normalizations of the parameter tensors, little influence.

    preproc_dict : dict
        Dict containing the keys 'win_len', 'hop_size' and 'C' necessary for STFT computation and post-processing.

    weights : list
        Weights by which the tensors of each layer are normalized, starting from the most shallow layer.

    init_data : array
        Initial value of the base signal. If None, array is drawn from standard normal distribution.

    exq_data : array
        If not None, this array is copied at the beginning of the base signal and not modified in the synthesis process.
         Intended for use with exquisite synthesis. If both exq_data and init_data are not None, their combined length
          must be equal to that of the target.

    lr : float
        Learning rate of the optimization. No effect if 'l-bfgs-b' is used.

    num_iter : int
        Number of iterations to be performed in the optimization.

    verbose : bool

    log_dir: Union[str, None]
        If set used to create a tensorboard log of the optimization.

    val_rand: bool
        If True a second model with the same structure but different weights will be run
        to test generalisation
    Returns
    -------
    synth : array
        Synthesized texture signal.

    conv_log : dict, optional
        Log of the optimization.
    """

    if "win_len" in preproc_dict:
        win_len = preproc_dict['win_len']
    else:
        win_len = np.cast[np.int32](np.round(preproc_dict['win_len_s'] * samp_rate))

    model = load_model(model_dir)
    model_config = model.get_config()

    if logger is not None:
        dinkus(logger)

    try:
        n_outputs = len(model.outputs)
    except TypeError:
        n_outputs = 1

    if weights is None:
        weights = np.ones(n_outputs, dtype=np.float32)
    elif len(weights) != n_outputs:
        raise RuntimeError('error: use weight list of size equal to number of activation layers')

    G_targ_list = []
    targ_shapes = []
    needed_model_outind = []

    target_std = None
    for filename in target_stats_files:
        targ_elem = iov.load_var(filename)
        if ("samp_rate" in targ_elem) and (targ_elem["samp_rate"] != samp_rate):
            logger.warn(f"sample rate {targ_elem['samp_rate']} in stats file {filename} and requested sound sample rate {samp_rate} do not match !!")
        if targ_elem["name"] == "std":
            target_std = targ_elem["stat"]
            continue
        try:
            index = model.output_names.index(targ_elem['name'])
        except ValueError:
            raise RuntimeError("failed to find model output corresponding to stat named {targ_elem['name']} ")
        G_targ_list.append(targ_elem["stat"])
        targ_shapes.append(targ_elem["shape"])
        needed_model_outind.append(index)

    needed_model_outind = np.array(needed_model_outind, dtype=np.int32)
    if np.any(needed_model_outind != np.arange(len(model.outputs))):
        # only keep the part of the model that is required
        logger.info(f"keep only the part of the model covering the stats from {[model.output_names[ii] for ii in needed_model_outind]}")
        model = keras.Model(inputs=model.inputs,
                            outputs=[model.outputs[ii] for ii in needed_model_outind])

    if logger is not None:
        model.summary(positions=[0.35, 0.68, 0.81, 1], line_length=85,
                      print_fn=logger.info)

    if exq_data is not None:
        exq_len = int(exq_data.shape[0])
    else:
        exq_len = 0


    if init_data is None:
        init = np.random.randn(syn_sig_len - exq_len).astype('float32')
        if exq_data is None:
            init[:win_len // 2] *= sig.hann(win_len)[:win_len // 2]
            init[-win_len // 2:] *= sig.hann(win_len)[-win_len // 2:]
        # init /= np.abs(stft(init, win_len, hop_size)).max()
    else:
        if init_data.shape[0] != syn_sig_len - exq_len:
            raise RuntimeError('error: invalid initialization size')
        init = init_data.astype('float32')


    if exq_data is not None:
        init = np.concatenate((exq_data.astype('float32'), init))

    synth = tf.Variable(init.astype('float32'), trainable=True, name="synth_inp")

    with tf.device("/CPU:0"):
        tf_preprocessor = TFPreprocessor(preproc_dict, logger=logger)

    hop_size =  tf_preprocessor.hop_size
    model_loss = ModelLoss(synth, model=model,
                           tf_preprocessor=tf_preprocessor,
                           weights=weights,
                           G_targ_list=G_targ_list,
                           targ_shapes=targ_shapes,
                           norm_mode=norm,
                           logger=logger,
                           progress_step=progress,
                           outfile_pat=outfile_pat,
                           outfile_red=outfile_red,
                           outfile_steps=outfile_steps,
                           commandline=commandline,
                           )
    # release memory
    del G_targ_list
    model_loss.calc_loss()
    model_loss.do_log(True)

    if logger is not None:
        dinkus(logger)
        logger.info('starting synthesis...')

    if exq_data is not None:
        window = np.ones(syn_sig_len)
        window[:exq_len] = 1 - sig.tukey(2 * exq_len)[exq_len:]
        window = window.astype('float32')
    else:
        window = None

    if num_iter:
        start_impose = time.time()
        if log_dir:
            summary_writer = tf.summary.create_file_writer(log_dir, flush_millis=5000)
        else:
            summary_writer = tf.summary.create_noop_writer()

        def logging_callback():
            tf.numpy_function(model_loss.do_log, inp=(), Tout=())

        model_loss.set_logging_callback(logging_callback=logging_callback)
        if num_iter:
            if opt == "l-bfgs-b":
                if False:
                    # this optimizer does not decrease monotonically,
                    # the lbfgs implementation is significantly simpler than the one used in scipy.optimize
                    # float32 precision ?
                    import tensorflow_probability as tfp
                    res = tfp.optimizer.lbfgs_minimize(
                        value_and_gradients_function=model_loss.value_and_grad_function_for_signal,
                        initial_position=model_loss.signal,
                        max_iterations=num_iter,
                        f_relative_tolerance=tf.constant(0, dtype=tf.float32),
                        x_tolerance=tf.constant(0, dtype=tf.float32))
                else:
                    import scipy.optimize as sopt
                    if gather_run_metadata:
                        tf.summary.trace_on(graph=True, profiler=True)
                        model_loss.value_and_grad_function()
                        with summary_writer.as_default():
                            tf.summary.trace_export(name="run_twinsyn_trace",
                                                    step=0,
                                                    profiler_outdir=log_dir)
                    else:
                        def func(signal):
                            return tuple(vv.numpy().astype(np.float64) for vv in
                                         model_loss.value_and_grad_function_for_signal(signal.astype(np.float32)))

                        with summary_writer.as_default():
                            fsig, floss, dd = sopt.fmin_l_bfgs_b(func=func,
                                                                 x0=model_loss.signal.numpy(), fprime=None, m=10,
                                                                 factr=1000,
                                                                 pgtol=1e-5 / np.prod(
                                                                     [int(ss) for ss in model_loss.signal.shape]),
                                                                 maxfun=num_iter * 100000, maxiter=num_iter)

                    if logger is not None:
                        logger.info("final loss: {}".format(floss, dd['task']))
                        logger.info("      task: {}".format(dd['task']))
                        logger.info("  funcalls: {}".format(dd["funcalls"]))
                        logger.info("       nit: {}".format(dd["nit"]))
            elif opt == "adam":
                optimizer = tf.keras.optimizers.Adam(learning_rate=adam_learning_rate)
                with summary_writer.as_default():
                    for epoch in range(num_iter):
                        loss, grad = model_loss.value_and_grad_function()
                        optimizer.apply_gradients(((grad, model_loss.signal),))
            else:
                raise RuntimeError("invalid optimizer {} selected.".format(opt))

        if logger is not None:
            logger.info('final loss: ' + str(model_loss.get_loss()))

        if num_iter:
            synth = model_loss.signal.numpy()

            # apply window to mask the border effects
            # The STFT does only use a single hop size to pad the signal at the end.
            # Therefore the signal borders up to 1/2 the window size + 1 hop size do not affect the stats and need
            # therefore to be removed.
            border_effect_window = np.hanning(win_len)
            synth[:win_len//2]    *= border_effect_window[:win_len//2]
            synth[-(win_len+hop_size):win_len//2-(win_len+hop_size)] *= border_effect_window[-(win_len//2):]
            synth[win_len // 2 - (win_len + hop_size):] = 0

            if target_std is not None:
                logger.info(f"fix std {np.std(synth)} -> {target_std}")
                synth *= target_std / np.std(synth)
        else:
            synth = None

        end_impose = time.time()
        if logger is not None:
            logger.info('synthesis complete')
            logger.info('time elapsed: synthesis {0:.2f}s'.format(round(end_impose - start_impose, 2)))

    # prepare to store in jason
    # convert log entries from tensorflow ListWrapper containing numpy numbers
    # into python lists containing python numbers
    conv_log = dict((kk, list((int(it) if isinstance(it, numbers.Integral) else float(it),
                               int(val) if isinstance(val, numbers.Integral) else float(val))
                              for it, val in vv))
                    for kk, vv in model_loss.conv_log.items())
    return synth, conv_log, model_config

