from as_pysrc.utils import nextpow2_val
import numpy as np
import os
import time
import tensorflow as tf

from ..as_pysrc.fileio import iovar as iov
from .util import read_snd
from .model import load_model, get_output_shapes_list

class TFPreprocessor(object):
    def __init__(self, preproc_dict, logger=None):
        """
        TF preprocessor: generates time frequency representtion of a signal and applies
        TF preporcessing and scaling
        Parameters
        ----------
        preproc_dict : dict
           configures preprocessing, should contain entries for
           win_len: int
           hop_size: int
           C : float
              scaling factor to be applied before sigmoid. If C<=0 the sigmoid compression
              will be replaced by means of a sqrt function applied individually to R and I channels without any scaling.

        """
        self.srate =  preproc_dict['samp_rate']
        if "win_len" in preproc_dict:
            self.win_len = preproc_dict['win_len']
            self.hop_size = preproc_dict['hop_size']
        else:
            self.win_len = np.cast[np.int32](np.round(preproc_dict['win_len_s'] * self.srate))
            self.hop_size = np.cast[np.int32](np.round(self.win_len/preproc_dict['oversamp']))

        self.C = preproc_dict['C']
        self.preproc_mode = "sigmoid"

        self.spec_norm_axis = None
        if "prep_mode" in preproc_dict:
            self.preproc_mode = preproc_dict['prep_mode']

        self.preproc_mode_none = tf.constant(0, dtype=tf.int32, shape=())
        self.preproc_mode_softsqrt = tf.constant(1, dtype=tf.int32, shape=())
        self.preproc_mode_approxsig = tf.constant(2, dtype=tf.int32, shape=())
        self.preproc_mode_sigmoid = tf.constant(3, dtype=tf.int32, shape=())

        self.tf_preproc_mode = None
        self.do_time_domain = False
        if self.preproc_mode == "none" or self.preproc_mode == "no":
            self.tf_preproc_mode = self.preproc_mode_none
        elif self.preproc_mode == "soft_sqrt" or self.preproc_mode == "so":
            self.tf_preproc_mode = self.preproc_mode_softsqrt
        elif self.preproc_mode == "approx_sigmoid" or self.preproc_mode == "ap":
            self.tf_preproc_mode = self.preproc_mode_approxsig
        elif self.preproc_mode == "sigmoid" or self.preproc_mode == "si":
            self.tf_preproc_mode = self.preproc_mode_sigmoid
        elif self.preproc_mode == "time_domaine" or self.preproc_mode == "ti":
            self.do_time_domain = True
        else:
            raise RuntimeError(
                "tf_get_tf_representation::error::unknown preprocessing mode {}".format(self.preproc_mode))

        self.equal_loudness_sr = None
        if "equal_loudness_sr" in preproc_dict:
            self.equal_loudness_sr = preproc_dict['equal_loudness_sr']
        if "spec_norm_axis" in preproc_dict:
            self.spec_norm_axis = preproc_dict['spec_norm_axis']

        self.fft_size = nextpow2_val(self.win_len)
        self.spec_norm = None
        self.equal_loud_SPL_tf = None
        if self.equal_loudness_sr is not None:
            from sig_proc.loudness_analysis import get_equal_loudness_curve_SPL
            fft_size = nextpow2_val(self.win_len)
            fft_freq_hz = np.arange(int(fft_size // 2 + 1)) * self.equal_loudness_sr / (fft_size)
            self.equal_loud_SPL_tf = tf.constant( np.sqrt(get_equal_loudness_curve_SPL(fft_freq_hz,
                                                                                       phonLevelRef=60).astype(np.float32))[:, np.newaxis])

    def __call__(self, tf_signal):
        """
        produce time frequency representation of input signal


        Parameters
        ----------
        tf_signal: tensorflow.Tensor


        Returns: tuple(array, float)
        -------
        """

        if not self.do_time_domain:
            temp = tf.signal.stft(tf_signal, self.win_len, self.hop_size)
            # Transpose into freq x time representation
            temp = tf.transpose(temp)

            if self.equal_loud_SPL_tf is not None:
                temp_R = tf.math.real(temp) / self.equal_loud_SPL_tf
                temp_I = tf.math.imag(temp) / self.equal_loud_SPL_tf
            else:
                temp_R = tf.math.real(temp)
                temp_I = tf.math.imag(temp)

            if self.spec_norm is None:
                self.spec_norm = tf.reduce_max(tf.sqrt(temp_R*temp_R + temp_I*temp_I), axis=self.spec_norm_axis,
                                          keepdims=False if self.spec_norm_axis is None else True)

            temp_R = temp_R / tf.cast(self.spec_norm, tf.float32)
            temp_I = temp_I / tf.cast(self.spec_norm, tf.float32)

            temp_RI = tf.stack([temp_R, temp_I], axis=-1)
            if self.tf_preproc_mode ==  self.preproc_mode_none:
                # self.preproc_mode == "none"
                temp_RI_comp = temp_RI
            elif self.tf_preproc_mode == self.preproc_mode_softsqrt:
                # self.preproc_mode == "soft_sqrt"
                temp_RI_comp = temp_RI / tf.sqrt(1 + tf.abs(temp_RI))
            elif self.tf_preproc_mode ==  self.preproc_mode_approxsig:
                # self.preproc_mode == "approx_sigmoid"
                temp_RI_comp = self.C * temp_RI / (1  + self.C * tf.abs(temp_RI))
            else:
                # self.preproc_mode == "sigmoid"
                temp_RI_comp = 2 * (tf.sigmoid(self.C * temp_RI) - 0.5)

            return temp_RI_comp
        else:
            return tf.expand_dims(tf.expand_dims(tf_signal, axis=-1), axis=0)

def G_from_F(F_targ):
    # F is the result of the convolution of the filter kernels over time and frequency axis.
    # the following matrix product multiplies the time dimension of the features between all pairs of filters
    # in the different feature channels (all filters have the same size).
    # frequency axis remains as additional dimension in the output and does not contribute to the
    # matrix product
    # which means that frequency features will never be displaced, and relations between frequency channels
    # are captured only by means of the scalar product between filters and time x frequency bins.
    # Reconstruction of the resulting Gram matrix will have to ensure
    # 1) that the output of the random time frequency filters will be reproduced
    # 2) that the frequency location is unchanged and
    # 3) that over time the correct correlation is ensured.
    #
    return tf.matmul(tf.transpose(F_targ[0], [0, 2, 1]), F_targ[0])


def calc_GRAM(ri_spec, model, name, as_list=False):
    tf_model_out = model(tf.expand_dims(ri_spec, axis=0))
    if not isinstance(tf_model_out, list):
        tf_model_out = [tf_model_out]
    if as_list:
        tarr = []
        for ii, F_targ in enumerate(tf_model_out):
            tarr.append(G_from_F(F_targ).numpy())
    else:
        tarr = tf.TensorArray(size=len(tf_model_out), name = name, dtype=tf.float32, infer_shape=False)
        for ii, F_targ in enumerate(tf_model_out):
            tarr = tarr.write(ii, G_from_F(F_targ))
    return tarr


def dump_cnn_stats(model_dir, preproc_dict, logger, target_files, dump_target_stats_dir, ffmpeg_bin="ffmpeg"):

    model = load_model(model_dir=model_dir)
    if dump_target_stats_dir:
        if not os.path.exists(dump_target_stats_dir):
            os.makedirs(dump_target_stats_dir)

    samp_rate = preproc_dict['samp_rate']
    if "win_len" in preproc_dict:
        win_len = preproc_dict['win_len']
    else:
        win_len = np.cast[np.int32](np.round(preproc_dict['win_len_s'] * samp_rate))

    with tf.device("/CPU:0"):
        tf_preprocessor = TFPreprocessor(preproc_dict, logger=logger)
        hop_size = tf_preprocessor.hop_size
        for target_file in target_files:
            logger.info(f"create stats for {target_file}")
            start = time.time()
            wav_array = read_snd(target_file, preproc_dict['samp_rate'], logger=logger, ffmpeg_bin=ffmpeg_bin)
            logger.info(f"read {target_file} with sample rate: {samp_rate}")
            tf_target = tf.constant(wav_array, dtype='float32')
            targ_perc = tf_preprocessor(tf_target)
            del tf_target
            targ_shapes = get_output_shapes_list(model, targ_perc)
            G_targ_list = calc_GRAM(ri_spec=targ_perc, model=model, name="GTARG", as_list=True)
            stats_dir = os.path.join(dump_target_stats_dir, os.path.splitext(os.path.basename(target_file))[0])
            if not os.path.exists(stats_dir):
                os.makedirs(stats_dir)
            wav_array_windowed = np.array(wav_array)
            border_effect_window = np.hanning(win_len)
            wav_array_windowed[:win_len//2]    *= border_effect_window[:win_len//2]
            wav_array_windowed[-(win_len+hop_size):win_len//2-(win_len+hop_size)] *= border_effect_window[-(win_len//2):]
            wav_array_windowed[win_len // 2 - (win_len + hop_size):] = 0
            wav_std =  np.std(wav_array_windowed)
            iov.save_var(os.path.join(stats_dir, "input.std.p"),
                         {"name": "std", "stat": wav_std, "shape": (), "snd_len": wav_array.size, "samp_rate" :samp_rate}, protocol=4)
            for stat, name, tshape in zip(G_targ_list, model.output_names, targ_shapes):
                iov.save_var(os.path.join(stats_dir, name + ".GRAM.p"),
                             {"name": name, "stat": stat, "shape": tshape, "snd_len" : wav_array.size, "samp_rate" :samp_rate}, protocol=4)

            end_targ = time.time()
            logger.info(f"created in {round(end_targ - start,2)}s")

    return
