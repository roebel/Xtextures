from as_pysrc.utils import nextpow2_val
import numpy as np
from typing import Dict
import os
from packaging import version
import json
import yaml
import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, AveragePooling2D, MaxPooling2D, Lambda, Concatenate, Activation, ELU, LeakyReLU
from tensorflow.keras.models import Model
from tensorflow.keras import activations

# ToDo ModelSelector is deprecated and will soon be removed
from .multi_cnn_architecture import ModelSelector

from .util import generate_weights, XtexturesLoader, dinkus

class elu_act(object):
    def __init__(self, alpha=1.):
        self._alpha = float(alpha)
        self._tf_alpha = tf.constant(float(alpha), dtype=tf.float32, shape=())
        self.use_default_elu = tf.constant(np.abs(alpha - 1) < 10*np.finfo(np.float32).eps, dtype=tf.bool, shape=())

    def __call__(self, input):
        if self.use_default_elu:
            return activations.elu(input)

        return activations.relu(input) + self._tf_alpha * (tf.exp(-activations.relu(-input)) - tf.constant(1, dtype=tf.float32,
                                                                                                           shape=()))

    def get_config(self):
        return {'alpha': self._alpha}

class leaky_act(object):
    def __init__(self, alpha=1.):
        self._alpha = float(alpha)
        self._tf_alpha = tf.constant(float(alpha), dtype=tf.float32, shape=())

    def __call__(self, input):
        return activations.relu(input, alpha=self._tf_alpha)

    def get_config(self):
        return {'alpha': self._alpha}


def replace_activation_function(kwargs : Dict, alpha = 0.2):

    if "activation" in kwargs:
        alpha = kwargs.pop("activation_alpha", alpha)
        if kwargs["activation"].lower() == "elu":
           kwargs["activation"] = elu_act(alpha=alpha)
        elif kwargs["activation"].lower() in ["leaky_relu", "leakyrelu"]:
            kwargs["activation"] = leaky_act(alpha=alpha)
        elif kwargs["activation"].lower() == "relu":
            kwargs["activation"] = activations.relu

    return

def create_model_from_structure(nbins, spec):
    input_shape = (nbins, None, 2)
    inputs = Input(input_shape)
    output_list = []
    model_spec = spec['model']
    for ll in model_spec["structure"]:
        temp = inputs
        for layer_spec in ll:
            if layer_spec["class"] == "Conv2D":
                kwargs = layer_spec["params"]
                if "shared_params" in layer_spec:
                    kwargs.update( layer_spec["shared_params"])
                replace_activation_function(kwargs=kwargs)
                temp = Conv2D(trainable=False, **kwargs)(temp)
            elif layer_spec["class"] == "AveragePooling2D":
                kwargs = layer_spec["params"]
                if "shared_params" in layer_spec:
                    kwargs.update(layer_spec["shared_params"])
                temp = AveragePooling2D(trainable=False, **kwargs)(temp)
            elif layer_spec["class"] == "MaxPooling2D":
                kwargs = layer_spec["params"]
                if "shared_params" in layer_spec:
                    kwargs.update( layer_spec["shared_params"])
                temp = AveragePooling2D(trainable=False, **kwargs)(temp)

        output_list.append(temp)

    return Model(inputs=inputs, outputs=output_list, name=model_spec["name"])

def create_cnn_model(model_name, dump_model_dir, preproc_dict, logger=None, model_args=None, activation="relu",
                     alpha=1., share_overlapping_weights= True, stft_coherent_weights=True, plot_file=None):

    if "win_len" in preproc_dict:
        win_len = preproc_dict['win_len']
    else:
        samp_rate =  preproc_dict['samp_rate']
        win_len = np.cast[np.int32](np.round(preproc_dict['win_len_s'] * samp_rate))

    fft_size = nextpow2_val(win_len)

    if model_name.endswith(".yaml") or model_name. endswith(".yml"):
        with open(model_name, 'r') as yaml_file:
            model_spec=yaml.load(stream=yaml_file, Loader=XtexturesLoader)
            # print(f"read model spec {model_spec}")
        if ("model" in model_spec) and ("structure" in model_spec["model"]):
            model = create_model_from_structure(nbins=fft_size//2+1, spec=model_spec)
        else:
            try:
                model = tf.keras.models.model_from_yaml(model_spec)
            except RuntimeError:
                if logger is not None:
                    logger.error(f"keras::error::since TF2.6 the construction of keras models from yaml files is no longer supported"
                                 f" you need to convert {model_name} into a json file!")
                raise

    elif model_name.endswith(".json"):
        with open(model_name, 'r') as json_file:
            model_spec = json.load(json_file)
            # print(f"read model spec {model_spec}")
            model = tf.keras.models.model_from_json(model_spec)
    else:
        selector = ModelSelector(fft_size//2 +1, activation=activation, alpha=alpha)
        try:
            if logger is not None:
                logger.info(f"model_name: {model_name}, model_args {model_args}")
            model = selector.getModel(model_name, model_args, logger=logger)

        except AttributeError as ex:
            raise RuntimeError(str(ex) + "\nand model '{}' is not a preconfigured model and cannot be found as json config file".format(model_name))

    layer_weight_inits = generate_weights(model, share_overlapping_weights, stft_coherent_weights, preproc_dict)
    model.set_weights(layer_weight_inits)


    if not os.path.exists(dump_model_dir):
        os.makedirs(dump_model_dir)

    #with open(os.path.join(dump_model_dir, "model_config.yaml"), "w") as file:
    #    file.write(model.to_yaml())
    with open(os.path.join(dump_model_dir, "model_config.json"), "w") as json_file:
        json_file.write(model.to_json(indent=2))

    model.save_weights(os.path.join(dump_model_dir, "model_weights.hdf5"))
    if plot_file:
        from tensorflow.keras.utils import plot_model
        if not plot_file[0] in ['.', '/']:
            plot_file = os.path.join(dump_model_dir, plot_file)
        try:
            plot_model(model, show_shapes=True, to_file=plot_file, rankdir="TD" if "-TD" in plot_file else "LR", dpi=200)
        except ValueError:
            pass
        model.summary()
    return


def load_model(model_dir, logger=None):
    model_config_file_yaml = os.path.join(model_dir, "model_config.yaml")
    model_config_file_json = os.path.join(model_dir, "model_config.json")
    if os.path.exists(model_config_file_json):
        has_yaml = False
    elif os.path.exists(model_config_file_yaml):
        if logger is not None:
            logger.warn("load_model::deprecation-warning::loading of keras models from yaml files is no longer supported in TF2.6")
            logger.warn(
            f"load_model::Xtextures now uses json format for storing model files. You better convert {model_config_file_yaml} into {model_config_file_json}.")
        has_yaml = True
    else:
        raise RuntimeError(f"cannot find any of the two possible model_config_files: {model_config_file_yaml} and {model_config_file_json}" )

    model_weights_file = os.path.join(model_dir, "model_weights.hdf5")
    if  not os.path.exists(model_weights_file):
        raise RuntimeError(f"cannot find model weights file {model_weights_file}")

    if logger is not None:
        dinkus(logger)

    if has_yaml:
        with open(model_config_file_yaml, 'r') as yaml_file:
            model_config_dict = yaml.load(stream=yaml_file, Loader=XtexturesLoader)
        if logger is not None:
            logger.info(f"loaded model config from {model_config_file_yaml}")
    else:
        with open(model_config_file_json, 'r') as json_file:
            model_config_dict = json.load(json_file)
        if logger is not None:
            logger.info(f"loaded model config from {model_config_file_json}")

    if "model_config" in model_config_dict:
        model_config = model_config_dict["model_config"]
    else:
        model_config = model_config_dict

    if ("keras_version" in model_config) and (version.parse(tf.keras.__version__) < version.parse(model_config["keras_version"])):
        logger.info(f"loaded generated with keras {model_config['keras_version']} into keras {tf.keras.__version__}")
        # support reading Xtextures models stored with TF 2.3 in TF 2.2
        if version.parse(tf.keras.__version__) < version.parse("2.4.0"):
            if model_config["class_name"] == "Functional" :
                model_config["class_name"] = "Model"
            def remove_entry(model_config, rk, dv):
                for k,v in model_config.items():
                    if isinstance(v, dict):
                        remove_groups(v, rk, dv)
                    elif k == "config"  and "groups" in v:
                        if v["groups"] == dv:
                            v.pop("groups")
                        else:
                            raise RuntimeError(f"cannot remove unsupported key {rk} because model config has "
                                               f"value {v['groups']} which is != the default value {dv}")
            remove_entry(model_config, "groups", 1)


    model = tf.keras.models.model_from_config(model_config)
    model.load_weights(model_weights_file)
    if logger is not None:
        logger.info(f"loaded model weights from {model_weights_file}")

    return model


def get_output_shapes_list(model, input):
    """
    generate the list of shapes of all model outouts for the given input
    Parameters
    ----------
    model: tf.keras.Model
    input: Union[tf.Tensor, tf.Variable, numpy.array]

    Returns
    -------
    a list of shapes for the different outputs
    if there is only a single output a list containing a single shape will be returned
    """
    in_shape = tuple(int(ss) for ss in input.shape)
    if len(in_shape) == 3:
        in_shape = (1,) + in_shape
    try:
        output_shapes = [[int(s2) for s2 in sh] for sh in model.compute_output_shape(in_shape)]
    except TypeError:
        output_shapes = [[int(s2) for s2 in model.compute_output_shape(in_shape)]]
    return output_shapes

