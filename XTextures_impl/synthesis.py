from ..as_pysrc.utils import nextpow2_val
import numpy as np
import scipy.signal as sig
import os
import time
import json
import tensorflow as tf
import tensorflow.keras as keras
import numbers
from .util import write_snd, read_snd

from ..as_pysrc.fileio import iovar as iov

from .util import dinkus, get_output_shapes_list
from .util import generate_weights

from .multi_cnn_architecture import ModelSelector

class TFPreprocessor(object):
    def __init__(self, preproc_dict, logger=None):
        """
        TF preprocessor: generates time frequency representtion of a signal and applies
        TF preporcessing and scaling
        Parameters
        ----------
        preproc_dict : dict
           configures preprocessing, should contain entries for
           win_len: int
           hop_size: int
           C : float
              scaling factor to be applied before sigmoid. If C<=0 the sigmoid compression
              will be replaced by means of a sqrt function applied individually to R and I channels without any scaling.

        """
        self.win_len = preproc_dict['win_len']
        self.hop_size = preproc_dict['hop_size']
        self.C = preproc_dict['C']
        self.preproc_mode = "sigmoid"
        self.srate =  preproc_dict['samp_rate']

        self.spec_norm_axis = None
        if "prep_mode" in preproc_dict:
            self.preproc_mode = preproc_dict['prep_mode']

        self.preproc_mode_none = tf.constant(0, dtype=tf.int32, shape=())
        self.preproc_mode_softsqrt = tf.constant(1, dtype=tf.int32, shape=())
        self.preproc_mode_approxsig = tf.constant(2, dtype=tf.int32, shape=())
        self.preproc_mode_sigmoid = tf.constant(3, dtype=tf.int32, shape=())

        self.tf_preproc_mode = None
        self.do_time_domain = False
        if self.preproc_mode == "none" or self.preproc_mode == "no":
            self.tf_preproc_mode = self.preproc_mode_none
        elif self.preproc_mode == "soft_sqrt" or self.preproc_mode == "so":
            self.tf_preproc_mode = self.preproc_mode_softsqrt
        elif self.preproc_mode == "approx_sigmoid" or self.preproc_mode == "ap":
            self.tf_preproc_mode = self.preproc_mode_approxsig
        elif self.preproc_mode == "sigmoid" or self.preproc_mode == "si":
            self.tf_preproc_mode = self.preproc_mode_sigmoid
        elif self.preproc_mode == "time_domaine" or self.preproc_mode == "ti":
            self.do_time_domain = True
        else:
            raise RuntimeError(
                "tf_get_tf_representation::error::unknown preprocessing mode {}".format(self.preproc_mode))

        self.equal_loudness_sr = None
        if "equal_loudness_sr" in preproc_dict:
            self.equal_loudness_sr = preproc_dict['equal_loudness_sr']
        if "spec_norm_axis" in preproc_dict:
            self.spec_norm_axis = preproc_dict['spec_norm_axis']

        self.fft_size = nextpow2_val(self.win_len)
        self.spec_norm = None
        self.equal_loud_SPL_tf = None
        if self.equal_loudness_sr is not None:
            from sig_proc.loudness_analysis import get_equal_loudness_curve_SPL
            fft_size = nextpow2_val(self.win_len)
            fft_freq_hz = np.arange(int(fft_size // 2 + 1)) * self.equal_loudness_sr / (fft_size)
            self.equal_loud_SPL_tf = tf.constant( np.sqrt(get_equal_loudness_curve_SPL(fft_freq_hz,
                                                                                       phonLevelRef=60).astype(np.float32))[:, np.newaxis])

    def __call__(self, tf_signal):
        """
        produce time frequency representation of input signal


        Parameters
        ----------
        tf_signal: tensorflow.Tensor


        Returns: tuple(array, float)
        -------
        """

        if not self.do_time_domain:
            temp = tf.signal.stft(tf_signal, self.win_len, self.hop_size)
            # Transpose into freq x time representation
            temp = tf.transpose(temp)

            if self.equal_loud_SPL_tf is not None:
                temp_R = tf.math.real(temp) / self.equal_loud_SPL_tf
                temp_I = tf.math.imag(temp) / self.equal_loud_SPL_tf
            else:
                temp_R = tf.math.real(temp)
                temp_I = tf.math.imag(temp)

            if self.spec_norm is None:
                self.spec_norm = tf.reduce_max(tf.sqrt(temp_R*temp_R + temp_I*temp_I), axis=self.spec_norm_axis,
                                          keepdims=False if self.spec_norm_axis is None else True)

            temp_R = temp_R / tf.cast(self.spec_norm, tf.float32)
            temp_I = temp_I / tf.cast(self.spec_norm, tf.float32)

            temp_RI = tf.stack([temp_R, temp_I], axis=-1)
            if self.tf_preproc_mode ==  self.preproc_mode_none:
                # self.preproc_mode == "none"
                temp_RI_comp = temp_RI
            elif self.tf_preproc_mode == self.preproc_mode_softsqrt:
                # self.preproc_mode == "soft_sqrt"
                temp_RI_comp = temp_RI / tf.sqrt(1 + tf.abs(temp_RI))
            elif self.tf_preproc_mode ==  self.preproc_mode_approxsig:
                # self.preproc_mode == "approx_sigmoid"
                temp_RI_comp = self.C * temp_RI / (1  + self.C * tf.abs(temp_RI))
            else:
                # self.preproc_mode == "sigmoid"
                temp_RI_comp = 2 * (tf.sigmoid(self.C * temp_RI) - 0.5)

            return temp_RI_comp
        else:
            return tf.expand_dims(tf.expand_dims(tf_signal, axis=-1), axis=0)

def G_from_F(F_targ):
    # F is the result of the convolution of the filter kernels over time and frequency axis.
    # the following matrix product multiplies the time dimension of the features between all pairs of filters
    # in the different feature channels (all filters have the same size).
    # frequency axis remains as additional dimension in the output and does not contribute to the
    # matrix product
    # which means that frequency features will never be displaced, and relations between frequency channels
    # are captured only by means of the scalar product between filters and time x frequency bins.
    # Reconstruction of the resulting Gram matrix will have to ensure
    # 1) that the output of the random time frequency filters will be reproduced
    # 2) that the frequency location is unchanged and
    # 3) that over time the correct correlation is ensured.
    #
    return tf.matmul(tf.transpose(F_targ[0], [0, 2, 1]), F_targ[0])


def calc_GRAM(ri_spec, model, name, as_list=False):
    tf_model_out = model(tf.expand_dims(ri_spec, axis=0))
    if not isinstance(tf_model_out, list):
        tf_model_out = [tf_model_out]
    if as_list:
        tarr = []
        for ii, F_targ in enumerate(tf_model_out):
            tarr.append(G_from_F(F_targ).numpy())
    else:
        tarr = tf.TensorArray(size=len(tf_model_out), name = name, dtype=tf.float32, infer_shape=False)
        for ii, F_targ in enumerate(tf_model_out):
            tarr = tarr.write(ii, G_from_F(F_targ))
    return tarr

class ModelLoss(tf.Module):
    def __init__(self, signal, model, tf_preprocessor, weights,
                 G_targ_list, targ_shapes, norm_mode,
                 logging_callback=None, progress_step = 5,
                 outfile_pat = None,
                 outfile_red = 0.7,
                 commandline = None,
                 logger=None):
        super().__init__()
        self.signal = signal
        self.model = model
        self.tf_preprocessor = tf_preprocessor
        self.weights= tf.convert_to_tensor(weights)
        self.targ_shapes = targ_shapes
        net_inp = tf_preprocessor(self.signal)
        self.syn_shapes = get_output_shapes_list(self.model, net_inp)
        self.norm_mode = norm_mode
        self.layer_names = [oo.name for oo in self.model.outputs]
        self.loss_list = ([tf.Variable(0, dtype=tf.float32, shape=(), name="loss", trainable=False)]
                          + [tf.Variable(0, dtype=tf.float32, shape=(), name="GL{0:>02d}".format(il), trainable=False)
                             for il, _ in enumerate(self.model.outputs)])

        self.logging_callback = logging_callback
        self.iteration = tf.Variable(0, dtype=tf.int32, shape=(), name="it")
        self.progress_step = progress_step
        self.logger = logger
        self.conv_log = {}
        self.last_outfile_loss = None
        self.num_outputs = len(self.layer_names)
        self.ta_G_targ_normed = None
        self.ta_gram_synth_norm = None
        self.loss_norm=None
        self.outfile_pat = outfile_pat
        self.outfile_red = outfile_red
        self.commandline = commandline
        self._prepare_gram_norm(G_targ_list=G_targ_list)

    def set_logging_callback(self, logging_callback):
        self.logging_callback = logging_callback

    def get_loss(self):
        return self.loss_list[0].numpy()

    @tf.function
    def value_and_grad_function(self):
        self.iteration.assign_add(1)
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            tape.watch(self.signal)
            loss = self.calc_loss()
        grad = tape.gradient(loss, self.signal)
        if self.logging_callback is not None:
            self.logging_callback()
        return loss, grad

    @tf.function
    def value_and_grad_function_for_signal(self, signal):
        self.signal.assign(signal)
        return self.value_and_grad_function()

    def do_log(self, force=False):
        it = self.iteration.numpy()
        for value in self.loss_list:
            name, valn  = value.name, value.numpy()
            if name not in self.conv_log:
                self.conv_log[name] = []
            self.conv_log[name].append((it, valn))
            tf.summary.scalar(name=name, data=valn, step=it)

        if self.logger is not None:
            if force or (self.progress_step==0 or (it % self.progress_step) == 0):
                logstr = [ "ep {0:4d}:".format(it)]
                for value in  self.loss_list:
                    logstr.append( "{0:4s} {1:.5g}".format(value.name[:4], value.numpy()))
                self.logger.info(logstr[0]+', '.join(logstr[1:]))

        if self.outfile_pat is not None:
            np_loss = self.loss_list[0].numpy()
            if self.last_outfile_loss is None:
                self.last_outfile_loss = np_loss
            elif self.last_outfile_loss * self.outfile_red  > np_loss:
                outfile= self.outfile_pat.format(it, np_loss)
                self.last_outfile_loss = np_loss
                if self.logger is not None:
                    self.logger.info(f"writing intermediate file: {outfile}")
                write_snd(outfile,
                          self.signal.numpy(), samp_rate=self.tf_preprocessor.srate, file_format="wav",
                          enc="float32", comments=self.commandline, logger=self.logger)

    def _prepare_gram_norm(self, G_targ_list):
        self.ta_G_targ_normed = tf.TensorArray(size=len(self.targ_shapes), dtype=tf.float32, infer_shape=False,
                                               clear_after_read=False)
        self.ta_gram_synth_norm = tf.TensorArray(size=len(self.targ_shapes), dtype=tf.float32, infer_shape=True,
                                                 clear_after_read=False, element_shape=())
        self.loss_norm = tf.Variable(0, dtype=tf.float32, shape=())
        for il, (G_targ, targ_shape, syn_shape) in enumerate(zip(G_targ_list, self.targ_shapes, self.syn_shapes)):
            if self.norm_mode == 'gatys':
                gram_targ_norm = np.prod(targ_shape[1:])
                gram_synth_norm = np.prod(syn_shape[1:])
            elif self.norm_mode == 'usty':
                temp = np.sqrt(np.sum(np.square(G_targ)))
                gram_targ_norm = temp
                gram_synth_norm = temp
            elif self.norm_mode == 'nusty':
                temp = np.sqrt(np.sum(np.square(G_targ)))
                gram_targ_norm= temp
                gram_synth_norm = (temp * syn_shape[2] / targ_shape[2])
            elif self.norm_mode is None:
                gram_targ_norm = 1.
                gram_synth_norm = 1.
            else:
                raise RuntimeError('synthTwinGram::error: {} is not a valid normalization'.format(self.norm_mode))
            self.ta_G_targ_normed = self.ta_G_targ_normed.write(il, G_targ / gram_targ_norm)
            self.ta_gram_synth_norm = self.ta_gram_synth_norm.write(il, gram_synth_norm)
            self.loss_norm.assign_add(self.weights[il] * np.sqrt(np.sum(np.square(G_targ))) / gram_targ_norm)


    def calc_loss(self):
        net_inp = self.tf_preprocessor(self.signal)
        G_synt_arr = calc_GRAM(ri_spec=net_inp, model=self.model, name="GSYN")
        loss_no_normed = tf.constant(0, dtype=tf.float32)
        for il in range(self.num_outputs):
            G_synt_normed = G_synt_arr.read(il)/ self.ta_gram_synth_norm.read(il)
            G_targ_normed = self.ta_G_targ_normed.read(il)
            err = tf.sqrt(tf.math.reduce_sum(tf.square((G_targ_normed - G_synt_normed))))
            self.loss_list[il+1].assign(err/self.loss_norm)
            loss_no_normed = loss_no_normed + self.weights[il] * err
        ret_val= loss_no_normed / self.loss_norm
        self.loss_list[0].assign(ret_val)
        return ret_val


def create_cnn_model(model_name, dump_model_dir, preproc_dict, logger=None, model_args=None, activation="elu",
                     alpha=1., share_overlapping_weights= True, stft_coherent_weights=True):

    win_len = preproc_dict['win_len']
    fft_size = nextpow2_val(win_len)

    if model_name.endswith("1D"):
        selector = ModelSelector(1, activation=activation, alpha=alpha)
    else:
        selector = ModelSelector(fft_size//2 +1, activation=activation, alpha=alpha)
    try:
        logger.info(f"model_name: {model_name}, model_args {model_args}")
        model = selector.getModel(model_name, model_args, logger=logger)

    except AttributeError as ex:
        raise RuntimeError(str(ex) + "\nand model '{}' is not a preconfigured model and cannot be found as json config file".format(model_name))

    layer_weight_inits = generate_weights(model, share_overlapping_weights, stft_coherent_weights, preproc_dict)
    model.set_weights(layer_weight_inits)

    model_config = { "class_name": model.__class__.__name__,
                     "config":model.get_config()
                     }

    if not os.path.exists(dump_model_dir):
        os.makedirs(dump_model_dir)
    with open(os.path.join(dump_model_dir, "model_config.json"), "w") as json_file:
        json.dump(model_config, json_file, indent=2)
    model.save_weights(os.path.join(dump_model_dir, "model_weights.hdf5"))

    return


def load_model(model_dir):
    model_config_file = os.path.join(model_dir, "model_config.json")
    if not os.path.exists(model_config_file):
       raise RuntimeError(f"cannot find model_config_file {model_config_file}" )

    model_weights_file = os.path.join(model_dir, "model_weights.hdf5")
    if  not os.path.exists(model_weights_file):
        raise RuntimeError(f"cannot find model weights file {model_config_file}")

    with open(model_config_file, 'r') as json_file:
        model_config_dict = json.load(json_file)

    if "model_config" in model_config_dict:
        model_config = model_config_dict["model_config"]
    else:
        model_config = model_config_dict

    model = keras.models.model_from_config(model_config)
    model.load_weights(model_weights_file)

    return model


def dump_cnn_stats(model_dir, preproc_dict, logger, target_files, dump_target_stats_dir, ffmpeg_bin="ffmpeg"):

    model = load_model(model_dir=model_dir)
    if dump_target_stats_dir:
        if not os.path.exists(dump_target_stats_dir):
            os.makedirs(dump_target_stats_dir)

    with tf.device("/CPU:0"):
        tf_preprocessor = TFPreprocessor(preproc_dict, logger=logger)
        for target_file in target_files:
            logger.info(f"create stats for {target_file}")
            start = time.time()
            wav_array = read_snd(target_file, preproc_dict['samp_rate'], logger=logger)
            tf_target = tf.constant(wav_array, dtype='float32')
            targ_perc = tf_preprocessor(tf_target)
            del tf_target
            targ_shapes = get_output_shapes_list(model, targ_perc)
            G_targ_list = calc_GRAM(ri_spec=targ_perc, model=model, name="GTARG", as_list=True)
            stats_dir = os.path.join(dump_target_stats_dir, os.path.splitext(os.path.basename(target_file))[0])
            if not os.path.exists(stats_dir):
                os.makedirs(stats_dir)
            wav_std =  np.std(wav_array)
            iov.save_var(os.path.join(stats_dir, "input.std.p"),
                         {"name": "std", "stat": wav_std, "shape": (), "snd_len": wav_array.size})
            for stat, name, tshape in zip(G_targ_list, model.output_names, targ_shapes):
                iov.save_var(os.path.join(stats_dir, name + ".GRAM.p"),
                             {"name": name, "stat": stat, "shape": tshape, "snd_len" : wav_array.size})

            end_targ = time.time()
            logger.info(f"created in {round(end_targ - start,2)}s")

    return


def impose_cnn_stats(model_dir, target_stats_files, norm='usty', preproc_dict=None, weights=None,
                  init_data=None, exq_data=None,
                  num_iter=1000, log_dir=None, gather_run_metadata=False, syn_sig_len=None,
                  progress=5, logger=None, opt="l-bfgs-b",
                  adam_learning_rate=0.001,  outfile_pat=None, outfile_red=0.7, commandline=None):
    """
    Synthesize sound texture signal from existing target texture using CNN-based parametric synthesis and RI
    representation: a base time signal is optimized until its parameter tensors draw close to those of the target
    texture.

    Parameters
    ----------
    target : Union[array, None]
        Target time signal. If given as None (or not given) the parameters target_stats_files, and init_data or syn_sig_len
        have to be provided

    model : keras model
        Model of the CNN used for synthesis. Convention must be chhanels-last ("Height-Width-Depth").

    norm : string
        Choice between 'gatys' and 'usty' normalizations of the parameter tensors, little influence.

    preproc_dict : dict
        Dict containing the keys 'win_len', 'hop_size' and 'C' necessary for STFT computation and post-processing.

    weights : list
        Weights by which the tensors of each layer are normalized, starting from the most shallow layer.

    init_data : array
        Initial value of the base signal. If None, array is drawn from standard normal distribution.

    exq_data : array
        If not None, this array is copied at the beginning of the base signal and not modified in the synthesis process.
         Intended for use with exquisite synthesis. If both exq_data and init_data are not None, their combined length
          must be equal to that of the target.

    lr : float
        Learning rate of the optimization. No effect if 'l-bfgs-b' is used.

    num_iter : int
        Number of iterations to be performed in the optimization.

    verbose : bool

    log_dir: Union[str, None]
        If set used to create a tensorboard log of the optimization.

    val_rand: bool
        If True a second model with the same structure but different weights will be run
        to test generalisation
    Returns
    -------
    synth : array
        Synthesized texture signal.

    conv_log : dict, optional
        Log of the optimization.
    """

    win_len = preproc_dict['win_len']

    model = load_model(model_dir)
    model_config = model.get_config()

    if logger is not None:
        model.summary(positions=[0.35, 0.68, 0.81, 1], line_length=85,
                      print_fn=logger.info)

    if logger is not None:
        dinkus(logger)
        logger.info('generate target Gram matrices ...')

    try:
        n_outputs = len(model.outputs)
    except TypeError:
        n_outputs = 1

    if weights is None:
        weights = np.ones(n_outputs, dtype=np.float32)
    elif len(weights) != n_outputs:
        raise RuntimeError('error: use weight list of size equal to number of activation layers')

    G_targ_list = []
    targ_shapes = []
    needed_model_outind = []

    target_std = None
    for filename in target_stats_files:
        targ_elem = iov.load_var(filename)
        if targ_elem["name"] == "std":
            target_std = targ_elem["stat"]
            continue
        try:
            index = model.output_names.index(targ_elem['name'])
        except ValueError:
            raise RuntimeError("failed to find model output corresponding to stat named {targ_elem['name']} ")
        G_targ_list.append(targ_elem["stat"])
        targ_shapes.append(targ_elem["shape"])
        needed_model_outind.append(index)

    needed_model_outind = np.array(needed_model_outind, dtype=np.int32)
    if np.any(needed_model_outind != np.arange(len(model.outputs))):
        # only keep the part of the model that is required
        logger.info(f"keep only the part of the model covering the stats from {[model.output_names[ii] for ii in needed_model_outind]}")
        model = keras.Model(inputs=model.inputs,
                            outputs=[model.outputs[ii] for ii in needed_model_outind])

    if exq_data is not None:
        exq_len = int(exq_data.shape[0])
    else:
        exq_len = 0


    if init_data is None:
        init = np.random.randn(syn_sig_len - exq_len).astype('float32')
        if exq_data is None:
            init[:win_len // 2] *= sig.hann(win_len)[:win_len // 2]
            init[-win_len // 2:] *= sig.hann(win_len)[-win_len // 2:]
        # init /= np.abs(stft(init, win_len, hop_size)).max()
    else:
        if init_data.shape[0] != syn_sig_len - exq_len:
            raise RuntimeError('error: invalid initialization size')
        init = init_data.astype('float32')


    if exq_data is not None:
        init = np.concatenate((exq_data.astype('float32'), init))

    synth = tf.Variable(init.astype('float32'), trainable=True, name="synth_inp")

    with tf.device("/CPU:0"):
        tf_preprocessor = TFPreprocessor(preproc_dict, logger=logger)

    model_loss = ModelLoss(synth, model=model,
                           tf_preprocessor=tf_preprocessor,
                           weights=weights,
                           G_targ_list=G_targ_list,
                           targ_shapes=targ_shapes,
                           norm_mode=norm,
                           logger=logger,
                           progress_step=progress,
                           outfile_pat=outfile_pat,
                           outfile_red=outfile_red,
                           commandline=commandline,
                           )
    # release memory
    del G_targ_list
    model_loss.calc_loss()
    model_loss.do_log(True)

    if logger is not None:
        dinkus(logger)
        logger.info('starting synthesis...')

    if exq_data is not None:
        window = np.ones(syn_sig_len)
        window[:exq_len] = 1 - sig.tukey(2 * exq_len)[exq_len:]
        window = window.astype('float32')
    else:
        window = None

    if num_iter:
        start_impose = time.time()
        if log_dir:
            summary_writer = tf.summary.create_file_writer(log_dir, flush_millis=5000)
        else:
            summary_writer = tf.summary.create_noop_writer()

        def logging_callback():
            tf.numpy_function(model_loss.do_log, inp=(), Tout=())

        model_loss.set_logging_callback(logging_callback=logging_callback)
        if num_iter:
            if opt == "l-bfgs-b":
                if False:
                    # this optimizer does not decrease monotonically,
                    # the lbfgs implementation is significantly simpler than the one used in scipy.optimize
                    # float32 precision ?
                    import tensorflow_probability as tfp
                    res = tfp.optimizer.lbfgs_minimize(
                        value_and_gradients_function=model_loss.value_and_grad_function_for_signal,
                        initial_position=model_loss.signal,
                        max_iterations=num_iter,
                        f_relative_tolerance=tf.constant(0, dtype=tf.float32),
                        x_tolerance=tf.constant(0, dtype=tf.float32))
                else:
                    import scipy.optimize as sopt
                    if gather_run_metadata:
                        tf.summary.trace_on(graph=True, profiler=True)
                        model_loss.value_and_grad_function()
                        with summary_writer.as_default():
                            tf.summary.trace_export(name="run_twinsyn_trace",
                                                    step=0,
                                                    profiler_outdir=log_dir)
                    else:
                        def func(signal):
                            return tuple(vv.numpy().astype(np.float64) for vv in
                                         model_loss.value_and_grad_function_for_signal(signal.astype(np.float32)))

                        with summary_writer.as_default():
                            fsig, floss, dd = sopt.fmin_l_bfgs_b(func=func,
                                                                 x0=model_loss.signal.numpy(), fprime=None, m=10,
                                                                 factr=1000,
                                                                 pgtol=1e-5 / np.prod(
                                                                     [int(ss) for ss in model_loss.signal.shape]),
                                                                 maxfun=num_iter * 100000, maxiter=num_iter)

                    if logger is not None:
                        logger.info("final loss: {}".format(floss, dd['task']))
                        logger.info("      task: {}".format(dd['task']))
                        logger.info("  funcalls: {}".format(dd["funcalls"]))
                        logger.info("       nit: {}".format(dd["nit"]))
            elif opt == "adam":
                optimizer = tf.keras.optimizers.Adam(learning_rate=adam_learning_rate)
                with summary_writer.as_default():
                    for epoch in range(num_iter):
                        loss, grad = model_loss.value_and_grad_function()
                        optimizer.apply_gradients(((grad, model_loss.signal),))
            else:
                raise RuntimeError("invalid optimizer {} selected.".format(opt))

        if logger is not None:
            logger.info('final loss: ' + str(model_loss.get_loss()))

        if num_iter:
            synth = model_loss.signal.numpy()

            if target_std is not None:
                logger.info(f"fix std {np.std(synth)} -> {target_std}")
                synth *= target_std / np.std(synth)
        else:
            synth = None

        end_impose = time.time()
        if logger is not None:
            logger.info('synthesis complete')
            logger.info('time elapsed: synthesis {0:.2f}s'.format(round(end_impose - start_impose, 2)))

    # prepare to store in jason
    # convert log entries from tensorflow ListWrapper containing numpy numbers
    # into python lists containing python numbers
    conv_log = dict((kk, list((int(it) if isinstance(it, numbers.Integral) else float(it),
                               int(val) if isinstance(val, numbers.Integral) else float(val))
                              for it, val in vv))
                    for kk, vv in model_loss.conv_log.items())
    return synth, conv_log, model_config


def synthTwinGram(target=None, model_name="getModel_Y06", norm='usty', preproc_dict=None, weights=None,
                  init_data=None, exq_data=None,
                  num_iter=1000, log_dir=None, gather_run_metadata=False, syn_sig_len = None,
                  progress=5, logger=None, model_args=None, val_rand=False, opt="l-bfgs-b", activation="elu",
                  alpha=1.,
                  adam_learning_rate=0.001, share_overlapping_weights= True,
                  stft_coherent_weights=True, outfile_pat=None, outfile_red=0.7, commandline=None,
                  model_weights_file=None,
                  dump_model_dir=None,
                  dump_target_stats_dir=None, target_stats_files=None):
    """
    Synthesize sound texture signal from existing target texture using CNN-based parametric synthesis and RI
    representation: a base time signal is optimized until its parameter tensors draw close to those of the target
    texture.
     
    Parameters
    ----------
    target : Union[array, None]
        Target time signal. If given as None (or not given) the parameters target_stats_files, and init_data or syn_sig_len
        have to be provided
        
    model : keras model
        Model of the CNN used for synthesis. Convention must be chhanels-last ("Height-Width-Depth").
        
    norm : string
        Choice between 'gatys' and 'usty' normalizations of the parameter tensors, little influence.
        
    preproc_dict : dict
        Dict containing the keys 'win_len', 'hop_size' and 'C' necessary for STFT computation and post-processing.
        
    weights : list
        Weights by which the tensors of each layer are normalized, starting from the most shallow layer.   
             
    init_data : array
        Initial value of the base signal. If None, array is drawn from standard normal distribution.
        
    exq_data : array
        If not None, this array is copied at the beginning of the base signal and not modified in the synthesis process.
         Intended for use with exquisite synthesis. If both exq_data and init_data are not None, their combined length
          must be equal to that of the target.
          
    lr : float
        Learning rate of the optimization. No effect if 'l-bfgs-b' is used.
        
    num_iter : int
        Number of iterations to be performed in the optimization.
        
    verbose : bool
    
    log_dir: Union[str, None]
        If set used to create a tensorboard log of the optimization.

    val_rand: bool
        If True a second model with the same structure but different weights will be run
        to test generalisation
    Returns
    -------
    synth : array
        Synthesized texture signal.
        
    conv_log : dict, optional
        Log of the optimization.
    """

    win_len = preproc_dict['win_len']
    fft_size = nextpow2_val(win_len)

    with tf.device("/CPU:0"):
        tf_preprocessor = TFPreprocessor(preproc_dict, logger=logger)
        if not target_stats_files:
            tf_target = tf.constant(target.astype('float32'))
            targ_perc = tf_preprocessor(tf_target)
            del tf_target
        if preproc_dict['hop_size'] != tf_preprocessor.hop_size:
            hop_fac = preproc_dict['hop_size'] / tf_preprocessor.hop_size

    model_config = None
    val_model = None

    if os.path.exists(os.path.join(model_name, "model_config.json")):
        if (not model_weights_file) and os.path.exists(os.path.join(model_name, "model_weights.hdf5")):
            model_weights_file = os.path.exists(os.path.join(model_name, "model_weights.hdf5"))
        model_name = os.path.join(model_name, "model_config.json")

    if os.path.exists(model_name) :
        with open(model_name, 'r') as json_file:
            # read json config as list of dictionaries to be able to separate the model
            # from the error function
            model_config_dict = json.load(json_file)

        if "model_config" in model_config_dict:
            #model_config = json.dumps(model_config_dict["model_config"])
            model_config = model_config_dict["model_config"]
        else:
            #model_config = json.dumps(model_config_dict)
            model_config = model_config_dict

        model = keras.models.model_from_config(model_config)
        if val_rand:
            val_model = keras.models.model_from_config(model_config)
    else:
        if preproc_dict["prep_mode"] == "time_domain" or preproc_dict["prep_mode"] == "ti":
            selector = ModelSelector(target.size, activation=activation, alpha=alpha)
        else:
            selector = ModelSelector(fft_size//2 +1, activation=activation, alpha=alpha)
        try:
            logger.info(f"model_name: {model_name}, model_args {model_args}")
            model = selector.getModel(model_name, model_args, logger=logger)
            if val_rand:
                val_model_args = dict(model_args)
                val_model = selector.getModel(model_name, kwdict=val_model_args, logger=logger)

        except AttributeError as ex:
            raise RuntimeError(str(ex) + "\nand model '{}' is not a preconfigured model and cannot be found as json config file".format(model_name))


    if model_config is None:
        model_config = model.get_config()

    if model_weights_file:
        model.load_weights(model_weights_file)
        if val_model:
            val_model.load_weights(model_weights_file)
    else:
        layer_weight_inits = generate_weights(model, share_overlapping_weights, stft_coherent_weights, preproc_dict)
        model.set_weights(layer_weight_inits)
        if val_model:
            val_model.set_weights(layer_weight_inits)

    if dump_model_dir:
        if not os.path.exists(dump_model_dir):
            os.makedirs(dump_model_dir)
        with open(os.path.join(dump_model_dir, "model_config.json"), "w") as json_file:
            json.dump(model_config, json_file, indent=2)
        model.save_weights(os.path.join(dump_model_dir, "model_weights.hdf5"))

        if not dump_target_stats_dir:
            return

    if logger is not None:
        model.summary(positions=[0.35, 0.68, 0.81, 1], line_length=85,
                      print_fn=logger.info)
        if val_model:
            val_model.summary(positions=[0.35, 0.68, 0.81, 1], line_length=85,  print_fn=logger.info)

    if logger is not None:
        dinkus(logger)
        logger.info('generate target Gram matrices ...')

    try:
        n_outputs = len(model.outputs)
    except TypeError:
        n_outputs = 1

    if weights is None:
        weights = np.ones(n_outputs, dtype=np.float32)
    elif len(weights) != n_outputs:
        raise RuntimeError('error: use weight list of size equal to number of activation layers')

    if not target_stats_files:
        start = time.time()
        with tf.device("/CPU:0"):
            targ_shapes = get_output_shapes_list(model, targ_perc)
            G_targ_list = calc_GRAM(ri_spec=targ_perc, model=model, name="GTARG", as_list=True)
        end_targ = time.time()

        if dump_target_stats_dir:
            if not os.path.exists(dump_target_stats_dir):
                os.makedirs(dump_target_stats_dir)

            for stat, name, tshape in zip(G_targ_list, model.output_names, targ_shapes):
                iov.save_var(os.path.join(dump_target_stats_dir, name + ".GRAM.p"),
                             {"name": name, "stat": stat, "shape": tshape})
            return
        # release memory
        del targ_perc
    else:
        G_targ_list = []
        targ_shapes = []
        needed_model_outind = []
        for filename in target_stats_files:
            targ_elem = iov.load_var(filename)
            try:
                index = model.output_names.index(targ_elem['name'])
            except ValueError :
                raise RuntimeError("failed to find model output corresponding to stat named {targ_elem['name']} ")
            G_targ_list[index] = targ_elem["stat"]
            targ_shapes[index] = targ_elem["shape"]
            needed_model_outind.append(index)

        needed_model_outind = np.array(needed_model_outind, dtype=np.int32)
        if needed_model_outind != np.arange(len(model.outputs)):
            # only keep the part of the model that is required
            print(f"keep only part of the model covering the model outputs {model.output_names[needed_model_outind]}")
            model = keras.Model(inuts=model.inputs,
                                outputs=model.outputs[needed_model_outind])

    if exq_data is not None:
        exq_len = int(exq_data.shape[0])
    else:
        exq_len = 0

    if syn_sig_len is None:
        syn_sig_len = target.shape[0]

    if init_data is None:
        init = np.random.randn(syn_sig_len - exq_len).astype('float32')
        if exq_data is None:
            init[:win_len//2] *= sig.hann(win_len)[:win_len//2]
            init[-win_len // 2:] *= sig.hann(win_len)[-win_len//2:]
        #init /= np.abs(stft(init, win_len, hop_size)).max()
        init *= np.std(target)/np.std(init)
    else:
        if init_data.shape[0] != syn_sig_len - exq_len:
            raise RuntimeError('error: invalid initialization size')
        init = init_data.astype('float32')

    if exq_data is not None:
        init = np.concatenate((exq_data.astype('float32'), init))

    synth = tf.Variable(init.astype('float32'), trainable=True, name = "synth_inp")
    model_loss = ModelLoss(synth, model=model,
                           tf_preprocessor=tf_preprocessor,
                           weights=weights,
                           G_targ_list=G_targ_list,
                           targ_shapes=targ_shapes,
                           norm_mode=norm,
                           logger=logger,
                           progress_step=progress,
                           outfile_pat=outfile_pat,
                           outfile_red = outfile_red,
                           commandline= commandline,
                           )
    # release memory
    del G_targ_list
    model_loss.calc_loss()
    model_loss.do_log(True)

    if logger is not None:
        dinkus(logger)
        logger.info('starting synthesis...')

    if exq_data is not None:
        window = np.ones(syn_sig_len)
        window[:exq_len] = 1 - sig.tukey(2 * exq_len)[exq_len:]
        window = window.astype('float32')
    else:
        window = None

    if num_iter:
        if log_dir:
            summary_writer = tf.summary.create_file_writer(log_dir, flush_millis=5000)
        else:
            summary_writer = tf.summary.create_noop_writer()

        def logging_callback():
            tf.numpy_function(model_loss.do_log, inp=(), Tout=())
        model_loss.set_logging_callback(logging_callback=logging_callback)
        if num_iter:
            if opt == "l-bfgs-b":
                if False:
                    # this optimizer does not decrease monotonically,
                    # the lbfgs implementation is significantly simpler than the one used in scipy.optimize
                    # float32 precision ?
                    import tensorflow_probability as tfp
                    res= tfp.optimizer.lbfgs_minimize(value_and_gradients_function=model_loss.value_and_grad_function_for_signal,
                                                 initial_position=model_loss.signal,
                                                 max_iterations=num_iter,
                                                 f_relative_tolerance=tf.constant(0, dtype=tf.float32),
                                                 x_tolerance=tf.constant(0, dtype=tf.float32))
                else:
                    import scipy.optimize as sopt
                    if gather_run_metadata:
                        tf.summary.trace_on(graph=True, profiler=True)
                        model_loss.value_and_grad_function()
                        with summary_writer.as_default():
                            tf.summary.trace_export( name="run_twinsyn_trace",
                                                     step=0,
                                                     profiler_outdir=log_dir)
                    else:
                        def func(signal):
                            return tuple(vv.numpy().astype(np.float64) for vv in
                                         model_loss.value_and_grad_function_for_signal(signal.astype(np.float32)))

                        with summary_writer.as_default():
                            fsig, floss, dd= sopt.fmin_l_bfgs_b(func=func,
                                                        x0=model_loss.signal.numpy(), fprime=None, m=10, factr=1000,
                                                        pgtol = 1e-5/np.prod([int(ss) for ss in model_loss.signal.shape]),
                                                        maxfun=num_iter*100000, maxiter=num_iter)


                    if logger is not None:
                        logger.info("final loss: {}".format(floss, dd['task']))
                        logger.info("      task: {}".format(dd['task']))
                        logger.info("  funcalls: {}".format(dd["funcalls"]))
                        logger.info("       nit: {}".format(dd["nit"]))
            elif opt == "adam":
                optimizer = tf.keras.optimizers.Adam(learning_rate=adam_learning_rate)
                with summary_writer.as_default():
                    for epoch in range(num_iter):
                        loss, grad = model_loss.value_and_grad_function()
                        optimizer.apply_gradients(((grad, model_loss.signal),))
            else:
                raise RuntimeError("invalid optimizer {} selected.".format(opt))

        if logger is not None:
            logger.info('final loss: '+ str(model_loss.get_loss()))

        if num_iter:
            synth = model_loss.signal.numpy()
        else:
            synth = None

    end_impose = time.time()
    if logger is not None:
        logger.info('synthesis complete')
        logger.info('time elapsed: target generation {0:.2f}s'.format(round(end_targ - start,2)))
        logger.info('time elapsed: synthesis {0:.2f}s'.format(round(end_impose - end_targ,2)))

    # prepare to store in jason
    # convert log entries from tensorflow ListWrapper containing numpy numbers
    # into python lists containg python numbers
    conv_log = dict((kk, list((int(it) if isinstance(it, numbers.Integral) else float(it),
                               int(val) if isinstance(val, numbers.Integral) else float(val))
                              for it,val in vv))
                    for kk,vv in model_loss.conv_log.items())
    return synth, conv_log, model_config
