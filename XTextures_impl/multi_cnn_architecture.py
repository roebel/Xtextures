
import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, AveragePooling2D, MaxPooling2D, Lambda, Concatenate, Activation, ELU, LeakyReLU
from tensorflow.keras.models import Model
from tensorflow.keras import activations
import tensorflow.keras.backend as K
import numpy as np

def makeUntrainable(layer):
    layer.trainable = False

    if hasattr(layer, 'layers'):
        for l in layer.layers:
            makeUntrainable(l)

def samplingLayer(mean, log_var):

    def sampling(args):
        mu, log_nu = args
        batch = K.shape(mu)[0]
        dim = K.int_shape(mu)[1]
        epsilon = K.random_normal(shape=(batch, dim))
        return mu + K.exp(0.5 * log_nu) * epsilon

    return Lambda(sampling)([mean, log_var])

class elu_act(object):
    def __init__(self, alpha=1.):
        self._alpha = float(alpha)
        self._tf_alpha = tf.constant(float(alpha), dtype=tf.float32, shape=())
        self.use_default_elu = tf.constant(np.abs(alpha - 1) < 10*np.finfo(np.float32).eps, dtype=tf.bool, shape=())

    def __call__(self, input):
        if self.use_default_elu:
            return activations.elu(input)

        return activations.relu(input) + self._tf_alpha * (tf.exp(-activations.relu(-input)) - tf.constant(1, dtype=tf.float32,
                                                                                                           shape=()))

    def get_config(self):
        return {'alpha': self._alpha}

class leaky_act(object):
    def __init__(self, alpha=1.):
        self._alpha = float(alpha)
        self._tf_alpha = tf.constant(float(alpha), dtype=tf.float32, shape=())

    def __call__(self, input):
        return activations.relu(input, alpha=self._tf_alpha)

    def get_config(self):
        return {'alpha': self._alpha}


class ModelSelector:

    def __init__(self, freq_size, activation="relu", alpha=1.):

        self.data_shape = (freq_size, None)
        self.activation = activation
        # this alpha parameter is used for certain activation functions, notably elu
        self.alpha = alpha

    def activation_function(self):
        if self.activation == "elu":
            return elu_act(alpha=self.alpha)
        elif self.activation == "leaky_relu":
            return leaky_act(alpha=self.alpha)

        return activations.relu

    def getModel_1D(self, num_filters=128):
        # multi layer 1D convolution to be applied directly on the signal

        input_shape = (*self.data_shape, 1)
        inputs = Input(input_shape)
        temp = inputs

        sizes   = [10] * 10
        strides = [2] * 10
        output_list = []

        for level, (size, stride) in enumerate(zip(sizes, strides)):
            temp = Conv2D(filters=num_filters, kernel_size=(1, size), strides=stride, padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_1D_fs{}_st{}_l{}".format(size, stride, level))(temp)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y00(self):
        # project Y
        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)
        sizes = [(29, 1), (2, 2), (3, 3), (5, 5), (7, 7), (11, 11), (17, 17), (1, 29)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False,
                          trainable=False, name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_Y03(self):
        # random small filters, 64 of several sizes, padding and concatenation, several scales

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)
        inp_xl = inputs
        inp_l = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_xl)
        inp_m = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_l)
        inp_s = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_m)

        inp_list = [inp_xl, inp_l, inp_m, inp_s]
        sizes = [3, 5, 7, 11]

        output_list = []

        for inp in inp_list:
            temp_list = []

            for size in sizes:
                temp = Conv2D(filters=64, kernel_size=(size, size), strides=(1, 1), padding='same', activation=None,
                              kernel_initializer='random_uniform', use_bias=False, trainable=False,
                              name="Conv2D_{}".format("_".join(str(s) for s in size)))(inp)

                temp_list.append(temp)

            conc = Concatenate()(temp_list)
            output = Activation(self.activation_function())(conc)
            output_list.append(output)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y04(self):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on smaller scale

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(3, 3), (11, 3), (53, 3), (101, 3), (5, 5), (7, 7), (11, 11), (11, 31)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_Y05(self):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and wide

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(101, 2),  (53, 3), (11, 5), (3, 3), (5, 5), (7, 7), (7, 31), (3, 53)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y06(self, num_filters=128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(101, 2),  (53, 3), (11, 5), (3, 3), (5, 5), (11, 11), (19, 19), (27, 27)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y06b(self, num_filters=128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(3, 3), (5, 5), (11, 11)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        size_v = (3,1)
        strides_v = (3,1)
        inpv = Conv2D(filters=9, kernel_size=size_v, strides=strides_v, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_v )))(inputs)

        sizes = [(33,2),  (14, 3), (4, 3)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inpv)

            output_list.append(temp)


        size_h = (1,3)
        strides_h = (1,3)
        inph = Conv2D(filters=9, kernel_size=size_h, strides=strides_h, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_h )))(inputs)

        sizes = [(3,6),  (4, 9)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inph)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y06bl(self, num_filters=128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(3, 3), (5, 5), (11, 11)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        size_v = (3,1)
        strides_v = (3,1)
        inpv = Conv2D(filters=18, kernel_size=size_v, strides=strides_v, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_v )))(inputs)

        sizes = [(33,2),  (14, 3), (4, 3)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inpv)

            output_list.append(temp)


        size_h = (1,3)
        strides_h = (1,3)
        inph = Conv2D(filters=18, kernel_size=size_h, strides=strides_h, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_h )))(inputs)

        sizes = [(3,6),  (4, 9)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inph)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y06c(self, num_prep_filters=9, num_filters=128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(3, 3), (5, 5), (11, 11)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        size_v = (9,1)
        strides_v = (3,1)
        inpv = Conv2D(filters=num_prep_filters, kernel_size=size_v, strides=strides_v, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_v )))(inputs)

        sizes = [(33,2),  (14, 3), (4, 3)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inpv)

            output_list.append(temp)


        size_h = (1,9)
        strides_h = (1,3)
        inph = Conv2D(filters=num_prep_filters, kernel_size=size_h, strides=strides_h, padding='valid',
                          activation=None,
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size_h )))(inputs)

        sizes = [(3,6),  (4, 9)]
        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inph)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)




    def getModel_Y07(self, num_filters = 128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes  = [(101, 2),  (53, 3), (11, 5), (3, 3), (5, 5), (7, 11), (7, 19), (7, 27),]
        output_list = []
        for size in  sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y07b(self, num_filters = 128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes  = [(101, 2),  (53, 3), (27, 5),(11, 5), (3, 3),  (5, 11), (7, 19), (7, 27),]
        output_list = []

        for size in  sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y07c(self, num_filters = 128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes  = [(101, 2),  (53, 3), (27, 3),(7, 7), (3, 3),  (3, 11), (3, 27), (3, 53)]
        output_list = []

        for size in  sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid',
                          activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_Y09(self, num_filters=128):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(100, 2),  (52, 3), (12, 5), (3, 3), (5, 5), (10, 10), (18, 18), (27, 27)]
        strides = [(4, 1),  (2, 1), (2, 1), (1, 1), (1, 1), (2, 1), (3, 1), (3, 1)]
        output_list = []

        for size, stride in zip(sizes, strides):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=stride, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_X00(self, num_filters=128):
        #  long time narrow freq, frequency correlation needs to be captured by means of cross
        # correlation Gram matrices covering depth and frequency dimension

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time correlations (horizontal)
        sizes = [(3, 3), (5, 5), (5, 11), (5, 19), (5, 27)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        # frequency correlations
        sizes     = [(3, 3),  (3, 3), (3, 3),  (3, 3), (3,2)]
        dilations = [(3, 1),  (7,1),  (11, 1), (23,1), (37,1)]
        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil),
                                                      ))(inputs)

            output_list.append(temp)



        return Model(inputs=inputs, outputs=output_list)


    def getModel_X01(self, num_filters=128):
        # using only 3x3 and 5x5 filters with dilations to achive the same receptive fields as Y06

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time correlations (horizontal)
        sizes     = [(3, 3),  (5, 5), (3, 3),  (3, 3), (3,3)]
        dilations = [(1, 1),  (1,1),  (4, 4), (6,6), (9,9)]
        output_list = []

        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil)))(inputs)

            output_list.append(temp)

        # frequency correlations (vertical
        sizes     = [(3, 3), (3, 1),   (3,1)]
        dilations = [(4, 2), (18, 1),  (34,1)]
        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil),
                                                      ))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_X02(self, num_filters=128):
        #  long time narrow freq, long freq narrow time  all achieved via dilations

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time correlations (horizontal)
        sizes     = [(3, 3),  (5, 5), (3, 3),  (3, 3), (3,3)]
        dilations = [(1, 1),  (1,1),  (2, 4), (2,6), (2,9)]
        output_list = []

        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil)))(inputs)

            output_list.append(temp)

        # frequency correlations (vertical
        sizes     = [(3, 3), (3, 1),   (3,1)]
        dilations = [(4, 2), (18, 1),  (34,1)]
        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil),
                                                      ))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_X03(self, num_filters=128):
        # using only 3x3 and 5x5 filters with dilations to achieve the same receptive fields as Y06
        # interestingly, this setup works very nicely, while the following X04 does converge
        # to much higher error level, and does not produce correct textures

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time correlations (horizontal)
        sizes     = [(3, 3),  (5, 5), (6, 6),  (6, 6), (7,7)]
        dilations = [(1, 1),  (1,1),  (2, 2), (3,3), (3,3)]
        output_list = []

        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil)))(inputs)

            output_list.append(temp)

        # frequency correlations (vertical
        sizes     = [(5, 5), (6, 1),  (11,1)]
        dilations = [(2, 1), (6, 1),  (9,1)]
        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil),
                                                      ))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_X04(self, num_filters=128):
        #  long time narrow freq, long freq narrow time  all achieved via dilations

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time correlations (horizontal)
        sizes     = [(3, 3),  (5, 5), (3, 6),  (3, 6), (3,7)]
        dilations = [(1, 1),  (1,1),  (2, 2), (2,3), (2,3)]
        output_list = []

        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil)))(inputs)

            output_list.append(temp)

        # frequency correlations (vertical
        sizes     = [(3, 3), (3, 1),   (3,1)]
        dilations = [(4, 2), (18, 1),  (34,1)]
        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil),
                                                      ))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_X05(self, num_filters=128):
        # smightly smoother increase of filter sizes and dilation factors compared to X04
        # works very similar to X03.

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        # time and frequency correlations
        sizes     = [(3, 3),  (5, 5), (6, 6),  (6,9), (5, 5), (6, 1), (8, 1), (12,1)]
        dilations = [(1, 1),  (1,1),  (2, 2),  (3,3), (2, 1), (3, 1), (4, 1), (8, 1)]
        output_list = []

        for size, dil in zip(sizes, dilations):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1,1), dilation_rate=dil, padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_d{}".format("_".join(str(s) for s in size),
                                                      "_".join(str(d) for d in dil)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_Y1L(self,
                     model_spec="101x2_128,53x3_128,11x5_128,3x3_128,5x5_128,11x11_128,19x19_128,27x27_128",
                     layer_name_prefix=""):
        """

        Parameters
        ----------
        model_spec: str
             A specification of the network separated by "|" or "-" for each sub model each of which consists
             of a colon separated list of CNN subnet specifications consisting of three integers of the form

             fs1xts1_nf1,fs2xts2_nf2,...

             with ts describing size of the time dimension of a filter, fs describing the size of the frequency dimension and
             nf the number of filters to be used for the respective subnets. Different subnets are separated by means
             of ":" or ","
             default model_spec reproduces Y06 model

             A sub model may be separated into a preprocessing and main network that are separated by letter N
             The preprocessing network works on the input and may be used to create a sub sampled signal that is subsequently feeded into
             main part of the sub network. The specs of the preprocesing network can in clude size and stride specs as follows

             fsxts_SfxSt_nf

             where fs, and ts are filter sizes in frequency and time direction and Sf, and St are strides in
             frequency and time direction. nf are the number of filters to be used for preprocessing

        Returnsip
        -------
        model: `Model`

        """
        # freely configurable set of random filters

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        output_list = []

        for ms in model_spec.replace("|", "-").split("-") :
            if "N" in ms:
                prep, fspec = ms.split("N")
                size_str, stride_str, nf = prep.split("_")
                f_size,t_size = size_str.split("x")
                f_stri,t_stri = stride_str.split("x")
                size = (int(f_size), int(t_size))
                stride = (int(f_stri), int(t_stri))
                net_input = Conv2D(filters=int(nf), kernel_size=size, strides=stride, padding='valid', activation=None,
                              kernel_initializer='random_uniform', use_bias=False, trainable=False,
                              name=layer_name_prefix+"Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)
            else:
                net_input = inputs
                fspec = ms

            for spec in fspec.replace(",", ":").split(":"):
                sstr,nf = spec.split("_")
                fs,ts = sstr.split("x")
                size = (int(fs), int(ts))
                temp = Conv2D(filters=int(nf), kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                              kernel_initializer='random_uniform', use_bias=False, trainable=False,
                              name=layer_name_prefix+"Conv2D_{}".format("_".join(str(s) for s in size)))(net_input)

                output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Y08(self, num_filters=128):
        # random small slim filters, 128 of several sizes, reduce redundancy compared to Y06 by means of removing all filters that overlap completely

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(101, 2),  (53, 3), (11, 5),  (27, 27)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel(self, tag, kwdict=None, logger=None):

        model_att = "getModel_"+tag
        if logger is not None:
            logger.info("construct model {} with params {}".format(model_att, str(kwdict)))

        if hasattr(self, model_att):
            from inspect import signature
            fun = getattr(self, model_att)
            sig = signature(fun)
            if logger is not None:
                logger.info(f"{sig}")
            if kwdict is not None and len(kwdict)> 0:
                args = dict()
                for key in kwdict:
                    if key in sig.parameters:
                        args[key] = kwdict[key]
                    else:
                        raise RuntimeError("unknown parameter {} provided for model {} args {}".format(key,
                                                                                                       model_att,
                                                                                                       [kk for kk in
                                                                                                        sig.parameters]))
                return fun(**args)
            else:
                return fun()
        raise AttributeError("{} does not exist in ModelSelector class.".format(model_att))


    def getModel_P01(self, num_filters=128):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)
        sizes = [(3, 3), (3, 1), (3, 1),  (3, 1), ]
        strides = [(2, 1), (2, 1), (2, 1),  (2, 1),]
        output_list = []
        temp = inputs
        for il, (size, stride) in enumerate(zip(sizes, strides)):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=stride, padding='valid', activation=None,
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_f{}".format(il)+"_{}".format("_".join(str(s) for s in size)))(temp)
            output_list.append(Activation(activation=self.activation_function())(temp))

        sizes = [(3, 3), (1, 3), (1, 3), (1, 3), ]
        strides = [(1, 2), (1, 2), (1, 2), (1, 2),]
        temp = inputs
        for il, (size, stride) in enumerate(zip(sizes, strides)):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=stride, padding='valid', activation=None,
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_t{}".format(il)+"_{}".format("_".join(str(s) for s in size)))(temp)
            output_list.append(Activation(activation=self.activation_function())(temp))

        sizes = [(3, 3), (3, 3), (3, 3)]
        strides = [(2, 2), (2, 2), (2, 2)]
        temp = inputs
        for il, (size, stride) in enumerate(zip(sizes, strides)):
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=stride, padding='valid', activation=None,
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_tf{}".format(il)+"_{}".format("_".join(str(s) for s in size)))(temp)
            output_list.append(Activation(activation=self.activation_function())(temp))


        return Model(inputs=inputs, outputs=output_list)

    def getModel_D01(self):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)
        sizes_f = [(3, 3)]
        dil_d = [(101, 2),  (53, 2), (11, 2), (5, 5), (11, 11), (19, 19), (27, 27)]
        output_list = []
        for size in sizes_f:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)
            output_list.append(temp)
        size = (2,2)
        for dil in dil_d:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), dilation_rate=dil,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil_{}".format("_".join(str(s) for s in size),
                                                          "_".join(str(s) for s in dil)))(inputs)
            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_D02(self):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        dilations = [(101, 2),  (53, 2), (11, 2), (5, 2), (11, 11), (19, 19), (27, 27)]
        output_list = []
        for size in sizes_f:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)
            output_list.append(temp)
        temp = MaxPooling2D(pool_size=sizes_f[0], strides=(1,1), padding="valid")(temp)
        size_d = (2,2)
        for dil in dilations:
            temp = Conv2D(filters=128, kernel_size=size_d, strides=(1, 1), dilation_rate=dil,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil_{}".format("_".join(str(s) for s in size_d),
                                                          "_".join(str(s) for s in dil)))(temp)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_D03(self):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only
        # max pool with stride in tme direction to reduce memory requirement and too high time precision
        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        dilations = [(101, 2),  (53, 2), (11, 2), (5, 2), (11, 5), (19, 7), (27, 11)]
        output_list = []
        for size in sizes_f:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)
            output_list.append(temp)
        size_d = (2,2)
        temp = MaxPooling2D(pool_size=sizes_f[0], strides=(1,3),  padding="valid")(temp)
        for dil in dilations:
            temp = Conv2D(filters=128, kernel_size=size_d, strides=(1, 1), dilation_rate=dil,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil_{}".format("_".join(str(s) for s in size_d),
                                                          "_".join(str(s) for s in dil)))(temp)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


    def getModel_D04(self):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only
        # max pool with stride in tme direction to reduce memory requirement and too high time precision
        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        output_list = []
        for size in sizes_f:
            in2 = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=None,
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)


        output_list.append(Activation(self.activation_function())(in2))
        dilations = [(3,3), (3,3), (3,3), (3,3), (3,3), (3,3), (3,1),   ]
        sizes     = [(2,2), (4,4), (6,6), (9,9), (4,2), (18,2), (37,1), ]
        for sz, dil in zip(sizes, dilations):
            temp = Conv2D(filters=128, kernel_size=sz, strides=(1, 1), dilation_rate=dil,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil_{}".format("_".join(str(s) for s in sz),
                                                          "_".join(str(s) for s in dil)))(in2)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_D05(self):
        # random small filters  and squared form, avoid redundancy by means of using dilation
        # and reduce filter coefficient to the filter edges only
        # max pool with stride in tme direction to reduce memory requirement and too high time precision
        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        output_list = []
        for size in sizes_f:
            in2 = Conv2D(filters=128, kernel_size=size, strides=(1, 2), padding='valid', activation=None,
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)


        output_list.append(Activation(self.activation_function())(in2))
        dilations = [(3,2), (3,2), (3,2), (3,2), (3,2), (3,2), (3,1),   ]
        sizes     = [(2,2), (4,4), (6,6), (9,9), (4,2), (18,2), (37,1), ]
        for sz, dil in zip(sizes, dilations):
            temp = Conv2D(filters=128, kernel_size=sz, strides=(1, 1), dilation_rate=dil,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil_{}".format("_".join(str(s) for s in sz),
                                                          "_".join(str(s) for s in dil)))(in2)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_D06(self):
        # Y06 with two levels
        # 3X3 filter standard form, all longer filters are derived from a single shared
        # prefiltering with depth 4 and 3X3 kernel with 3x3 strides

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        output_list = []
        for size in sizes_f:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                         kernel_initializer='random_uniform', use_bias=False, trainable=False,
                         name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

        output_list.append(temp)
        insize=(3,3)
        in3 = Conv2D(filters=1, kernel_size=insize, strides=(1, 1), padding='valid', activation=None,
                         kernel_initializer='random_uniform', use_bias=False, trainable=False,
                         name="Conv2D_in3_{}".format("_".join(str(s) for s in insize)))(inputs)
        sizes = [(2, 2), (4, 4), (6, 6), (9, 9), (4, 2), (18, 1), (37, 1), ]
        for sz in sizes:
            temp = Conv2D(filters=128, kernel_size=sz, strides=(1,1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_in3_{}".format("_".join(str(s) for s in sz)))(in3)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_D07(self):
        # Y06 with two levels
        # 3X3 filter standard form, all longer filters are created in two stages via
        # prefiltering with depth 4 and 3X3 kernel with 3x3 strides, prefilters are not shared

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes_f = [(3, 3)]
        output_list = []
        for size in sizes_f:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                         kernel_initializer='random_uniform', use_bias=False, trainable=False,
                         name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

        output_list.append(temp)
        instrides = [(1, 1), (1, 1), (3, 3), (3, 3), (1, 1), (3, 1), (3, 1), ]
        sizes     = [(2, 2), (4, 4), (6, 6), (9, 9), (4, 2), (18, 1), (37, 1), ]
        outdil    = [(3, 3), (3, 3), (1, 1), (1, 1), (3, 3), (1, 1), (1, 1), ]
        for sz,ins, outd in zip(sizes, instrides, outdil):
            insize = (3, 3)
            temp = Conv2D(filters=1, kernel_size=insize, strides=ins, padding='valid', activation=None,
                         kernel_initializer='random_uniform', use_bias=False, trainable=False,
                         name="Conv2D_pre_{}".format("_".join(str(s) for s in sz)))(inputs)
            temp = Conv2D(filters=128, kernel_size=sz, strides=(1,1), dilation_rate=outd,padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in sz)))(temp)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_E01(self, num_filters=128):
        # Multi layer structure preserving approximately the receptive fields of the different layers of Y06
        # using avg pooling after the first 3x3 filter layer to reduce computational complexity
        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        size_f = (3, 3)
        # YO0 sizes = [(101, 2),  (53, 3), (11, 5), (3, 3), (5, 5), (11, 11), (19, 19), (27, 27)]
        sizes_m = [     (33, 1),  (17, 1), (3, 2),          (3, 3), (4, 4),  (7, 7),    (9, 9)]

        output_list = []
        temp = Conv2D(filters=num_filters, kernel_size=size_f, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}".format("_".join(str(s) for s in size_f)))(inputs)
        output_list.append(temp)
        temp = AveragePooling2D(pool_size=size_f, strides=(3,3),  padding="valid")(temp)
        for size in sizes_m:
            temp = Conv2D(filters=num_filters, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_AP33_{}".format("_".join(str(s) for s in size)))(temp)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_test(self):
        # random small slim filters, 128 of several sizes, no concatenation, more focus on high and square

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(7, 7), (11, 11)]
        output_list = []

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Z00(self):
        # multi-scale filters, RI spec

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        inp_xl = inputs
        inp_l = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_xl)
        inp_m = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_l)
        inp_s = AveragePooling2D(pool_size=(2, 2), strides=(2, 2))(inp_m)

        inp_list = [inp_xl, inp_l, inp_m, inp_s]

        sizes = [(17, 1),  (3, 3), (1, 5)]
        output_list = []

        for inp in inp_list:
            for size in sizes:
                temp = Conv2D(filters=32, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                              kernel_initializer='random_uniform', use_bias=False, trainable=False,
                              name="Conv2D_{}".format("_".join(str(s) for s in size)))(inp)
                output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Z01(self):
        # hollow Y06

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)


        sizes = [(2, 2), (4, 4), (6, 6), (12, 12), (32, 1), (18, 2)]
        output_list = []

        size = (3,3)
        temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_str1".format("_".join(str(s) for s in size)))(inputs)

        output_list.append(temp)

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, padding='valid', dilation_rate=(3, 3), activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil3".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Z02(self):
        # hollow light Y06

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)

        sizes = [(2, 2), (4, 4), (6, 6), (12, 12), (32, 1), (18, 2)]
        output_list = []

        size = (3,3)
        temp = Conv2D(filters=18, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                      kernel_initializer='random_uniform', use_bias=False, trainable=False,
                      name="Conv2D_{}_str1".format("_".join(str(s) for s in size)))(inputs)

        output_list.append(temp)

        for size in sizes:
            temp = Conv2D(filters=2*np.prod(size), kernel_size=size, padding='valid', dilation_rate=(3, 3),
                          activation=self.activation_function(), kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil3".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Z03(self):
        # multi-scale filters, RI spec

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)
        inp_m = inputs
        inp_s = AveragePooling2D(pool_size=(3, 3), strides=(2, 2))(inp_m)

        sizes_m = [(101, 2),  (3, 3)]
        sizes_s = [(2, 2), (4, 4), (6, 6), (12, 12), (18, 1)]
        output_list = []

        for size in sizes_m:
            temp = Conv2D(filters=32, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}m".format("_".join(str(s) for s in size)))(inp_m)
            output_list.append(temp)

        for size in sizes_s:
            temp = Conv2D(filters=32, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}s".format("_".join(str(s) for s in size)))(inp_s)
            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)

    def getModel_Z04(self):
        # hollow Y06 with full small filters

        input_shape = (*self.data_shape, 2)
        inputs = Input(input_shape)


        full_sizes = [(3, 3), (5, 5)]
        sizes = [(4, 4), (6, 6), (12, 12), (32, 1), (18, 2)]
        output_list = []

        for size in full_sizes:
            temp = Conv2D(filters=128, kernel_size=size, strides=(1, 1), padding='valid', activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil1".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        for size in sizes:
            temp = Conv2D(filters=128, kernel_size=size, padding='valid', dilation_rate=(3, 3), activation=self.activation_function(),
                          kernel_initializer='random_uniform', use_bias=False, trainable=False,
                          name="Conv2D_{}_dil3".format("_".join(str(s) for s in size)))(inputs)

            output_list.append(temp)

        return Model(inputs=inputs, outputs=output_list)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("store model config file fo later use in ")
    parser.add_argument("model_tag", help="model selector tag to be used")

