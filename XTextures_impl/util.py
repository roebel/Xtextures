import os
import sys
import numpy as np

import yaml
import json
import re

import tensorflow as tf

from ..as_pysrc.utils import create_logger as cl
from ..as_pysrc.sig_proc.resample import resample
import scipy.io.wavfile as scio_wav
import subprocess as sp
try:
    from pysndfile import sndio, fileformat_name_to_id
except (ImportError, ModuleNotFoundError) :
    sndio = None


class XtexturesLoader(yaml.SafeLoader):
    def construct_python_tuple(self, node):
        return tuple(self.construct_sequence(node))

XtexturesLoader.add_constructor(
    u'tag:yaml.org,2002:python/tuple',
    XtexturesLoader.construct_python_tuple)

def norm_if(x, limit=0.99999):
    """Normalise to one if the signal would cause clipping"""
    norm = np.max(np.abs(x))
    if norm > limit:
        return x / norm * limit
    return x

def get_stft_patch(need_len, preproc_dict):
    """
    get an STFT patch in RI representation with  at least need_len number of frames
    Parameters
    ----------
    need_len
    preproc_dict

    Returns
    -------

    """
    from sig_proc.spec import STFT

    win_len = preproc_dict['win_len']
    hop_size = preproc_dict['hop_size']

    ss = np.random.randn(need_len * hop_size + 2* win_len)
    stft_obj = STFT(ss, win=np.hanning(win_len), step=hop_size, detrend_phase=False, extend_outside=False)
    dd = stft_obj.get_data().T
    dd_RI = np.stack([np.real(dd), np.imag(dd)], axis=-1)
    return dd_RI

def generate_weights(model, share_overlapping_weights, stft_coherent_weights, preproc_dict):
    """
    generate weights using numpy.random.uniform for a RI CNN multi network model
    Parameters
    ----------
    model : `keras.models.Model`
    share_overlapping_weights : bool
       if True all weights are produced wffrom a single patch of a size that
       covers all filters
    stft_coherent_weights : bool
       if True the generator does not use uniform random numbers but patches of the
       RI STFT of a uniformly distributed andom signal
    preproc_dict : dict
       STFT preprocessing parameters used for the imposition, will be used only in case stft_coherent_weights is TRue
    Returns
    -------
    layer_weight_inits :  list
        list of numpy arrays corresponding to teh layers in model that require initialization
    """
    layer_weight_inits = []
    max_sizes = None
    if share_overlapping_weights:
        max_sizes = None
        for lw in model.weights:
            shape = [int(ss) for ss in lw.shape]
            if max_sizes is None:
                max_sizes = shape
            else:
                for ind, (ms, ss) in enumerate(zip(max_sizes, shape)):
                    if ss > ms:
                        max_sizes[ind] = ss

    if stft_coherent_weights:
        if share_overlapping_weights:
            weight_inits = np.zeros(max_sizes)
            for id in range(max_sizes[-1]):
                patch = get_stft_patch(max_sizes[1], preproc_dict=preproc_dict)
                off = np.random.random_integers(0, patch.shape[0] - max_sizes[0], 1)
                weight_inits[:, :, :, id] = patch[off: off + max_sizes[0], :max_sizes[1], :]

            for lw in model.weights:
                slices = tuple(slice(0, int(ss)) for ss in lw.shape)
                layer_weight_inits.append(weight_inits[slices])

        else:
            for lw in model.weights:
                w_height = int(lw.shape[0])
                w_len = int(lw.shape[1])
                w_depth = int(lw.shape[2])
                n_weights = int(lw.shape[-1])
                patch = get_stft_patch(w_len * n_weights, preproc_dict=preproc_dict)
                weight_inits = np.zeros((w_height, w_len, w_depth, n_weights))

                print(w_height, w_len, w_depth, n_weights, patch.shape)
                for id in range(n_weights):
                    off = np.random.random_integers(0, patch.shape[0] - w_height)
                    # print(w_len, id*w_len, (id+1)*w_len)
                    weight_inits[:, :, :, id] = patch[off:off + w_height, id * w_len:(id + 1) * w_len, :]

                layer_weight_inits.append(weight_inits)

    else:
        if share_overlapping_weights:
            weight_inits = np.random.uniform(-0.05, 0.05, size=max_sizes)

            for lw in model.weights:
                slices = tuple(slice(0, int(ss)) for ss in lw.shape)
                layer_weight_inits.append(weight_inits[slices])

        else:
            for lw in model.weights:
                sizes = tuple(int(ss) for ss in lw.shape)
                layer_weight_inits.append(np.random.uniform(-0.05, 0.05, size=sizes))

    return layer_weight_inits


def dinkus(logger):
    if logger is not None:
        logger.info('-----------------------------------------------------------------')


def lockGPU(use_gpu= True, logger=None):
    """
    Locks a GPU.
    """
    gpl = None
    gpu_device_id = None
    # gpu_ids will be None on systems without gpu nvidia card
    if use_gpu:

        try:
            import manage_gpus as gpl
            try:
                gpu_ids=gpl.board_ids()
                if gpu_ids is not None:
                    gpu_device_id = -1
                else:
                    raise RuntimeError("train_onsets::error:: no gpu devices available on this system, you cannot select a gpu")
            except gpl.NoGpuManager:
                gpu_device_id = 0

        except ImportError:
            pass
    else:
        if hasattr(tf.config, "set_visible_devices"):
            tf.config.set_visible_devices([], 'GPU')
        else:
            tf.config.experimental.set_visible_devices([], 'GPU')
        return "/cpu:0"

    # now we lock a GPU because we will need one
    if (gpu_device_id is not None) and (gpl is not None):
        try:
            gpl.get_gpu_lock(gpu_device_id = gpu_device_id, soft=False)
        except gpl.NoGpuManager:
            if logger is not None:
                logger.info("no gpu manager available - will use all available GPUs")

    return "/GPU:0"


def freeGPU(gpu_id_locked):
    """
    Frees specified GPU: commented because of no use with current version of manage_gpus.
    """

    # import manage_gpus as gpl
    # gpl.free_lock(gpu_id_locked)
    pass


_fir_filters = {}

def read_snd(snd_file, samp_rate=22050, dtype='float32',
             logger=None, ffmpeg_bin='ffmpeg'):

    if logger is not None:
        logger.info(f'read {snd_file}')

    snd = None
    if sndio is not None:
        try:
            snd, in_srate, _ = sndio.read(snd_file, dtype=np.dtype(dtype))
        except Exception as ex:
            if isinstance(ex, OSError) and ("unknown format" in ex.args[0]):
                logger.info("pysdfile::error::unsupported format -> try to read with ffmpeg")
            else:
                raise
    if snd is None:
        snd, log_info, in_srate = readSnd_ffmpeg(snd_file=snd_file, ffmpeg_bin=ffmpeg_bin)
        logger.info(log_info.decode("UTF8"))

    if (in_srate != samp_rate):
        if (in_srate, samp_rate) in _fir_filters:
            fir = _fir_filters[(in_srate, samp_rate)]
            snd, _ = resample(snd, in_sr=in_srate, out_sr=samp_rate, fir_filt=fir)
        else:
            fir = None
            snd, fir = resample(snd, in_sr=in_srate, out_sr=samp_rate, fir_filt=fir)
            _fir_filters[(in_srate, samp_rate)] = fir
    else:
        samp_rate = in_srate
    return snd

def readSnd_ffmpeg(snd_file, ffmpeg_bin='ffmpeg'):
    """
    Returns the sound file  as a numpy array with dtype float32 using ffmpeg - supports mp3.

    Parameters
    ----------
    snd_file : string
        Path to the snd file to be downloaded.
        
    ffmpeg_bin : string
        Path of the ffmpeg binary file. [default: 'ffmpeg']
        

    Returns
    -------
    audio_array : 1D array
        Numpy array version of the snd input.
    log_info : bytearray
        log output generated by ffmpeg
    """

    probecommand = [ffmpeg_bin, '-i', snd_file]
    pipe = sp.Popen(probecommand, stderr=sp.PIPE, stdout=sp.PIPE, bufsize=10 ** 8, universal_newlines=True)

    _ = pipe.stdout.read()
    probe_info = pipe.stderr.read()

    for ll in probe_info.split("\n"):
        if "Audio" in ll and "Hz" in ll:
            srate=int(re.sub(".*, *([0-9]*) *Hz,.*", r"\1", ll).strip())


    command = [ffmpeg_bin, '-i', snd_file, '-f', 'f32le', '-acodec', 'pcm_f32le', '-']
    pipe = sp.Popen(command, stderr=sp.PIPE, stdout=sp.PIPE, bufsize=10 ** 8)

    raw_audio = pipe.stdout.read()
    log_info = pipe.stderr.read()
    snd = np.frombuffer(raw_audio, dtype="float32")
    if np.isnan(snd).any():
        raise RuntimeError(f'readSnd_ffmpeg:error:: detecetd NaN audio data in {snd_file}!')

    return snd, log_info, srate

def finalize_job(ret, synth_path, output_name, args, preproc_dict, ilog, command, out_log_file,
                 default_log_format, count, synth_num, rem_file_logger=True):
    if ret is not None:
        synth, conv_log, model_config_dict = ret

        osndfile = os.path.join(synth_path, output_name) + "." + args.format
        ilog.info('write: {}'.format(osndfile))
        write_snd(osndfile, norm_if(synth), samp_rate=preproc_dict['samp_rate'], file_format=args.format,
                  enc=args.enc, comments=command, logger=ilog)
        #with open(os.path.join(synth_path, output_name.split('.')[0] + '_conv_log.yaml'), 'w') as outfile:
        #    yaml.dump(conv_log, stream=outfile, default_flow_style=False, indent=4)

        if np.max(np.abs(synth)) >= 1:
            write_snd(os.path.join(synth_path, output_name) + "nonorm_float32.wav",
                      synth, samp_rate=preproc_dict['samp_rate'], file_format="wav",
                      enc="float32", comments=command, logger=ilog)
        if model_config_dict:
            if False:
                with open(os.path.join(synth_path, output_name + ".model.json"), "w") as json_file:
                    json.dump(model_config_dict, json_file, indent=2)
            with open(os.path.join(synth_path, output_name + ".model.yaml"), "w") as fo:
                yaml.dump(model_config_dict, stream=fo, default_flow_style=False, indent=4)

        if rem_file_logger:
            cl.rem_file_logger(ilog, out_log_file)
        cl.set_stream_message_format(ilog, log_format=default_log_format)
        ilog.info('synthesis {} out of {} complete.'.format(count, synth_num))
    else:
        ilog.info('process {} out of {} complete.'.format(count, synth_num))

def write_snd(filename, vec, samp_rate, file_format, enc, comments=None, logger=None):
    """
    write out sound file including comments
    :param filename:
    :param vec:
    :param samp_rate:
    :param file_format:
    :param enc:
    :param comments:
    :return:
    """

    if file_format is None:
        ext = os.path.splitext(filename)[1]
        if len(ext)> 1 :
            ext_no_dot = ext[1:]
            for fmt in fileformat_name_to_id:
                if fmt == ext_no_dot or fmt == ext_no_dot[:3]:
                    file_format = fmt

    if (not os.path.isdir(os.path.dirname(filename))):
        os.makedirs(os.path.dirname(filename), exist_ok=True)

    if sndio is not None:
        if comments:
            try:
                try:
                    sndio.write(filename, vec, rate=samp_rate, format=file_format, enc=enc,
                                sf_strings={"SF_STR_COMMENT": bytes(comments, encoding="ASCII")})
                    return
                except TypeError:
                    sndio.write(filename, vec, rate=samp_rate, format=file_format, enc=enc,
                                sf_strings={"SF_STR_COMMENT": comments})
                    return
            except (RuntimeError, AttributeError):
                if logger is not None:
                    logger.warn("warning:cannot add string-comment to {0}".format(filename))

        sndio.write(filename, vec, rate=samp_rate, format=file_format, enc=enc)
    else:
        if file_format != "wav":
            raise RuntimeError(f"write_snd::error:: pysndfile support is not available only 'wav' snds can be written. "
                               f"Select a file with extension '.wav'")

        scio_wav.write(filename, rate=samp_rate, data=vec.astype(np.float32))



def get_output_shapes_list(model, input):
    """
    generate the list of shapes of all model outouts for the given input
    Parameters
    ----------
    model: tf.keras.Model
    input: Union[tf.Tensor, tf.Variable, numpy.array]

    Returns
    -------
    a list of shapes for the different outputs
    if there is only a single output a list containing a single shape will be returned
    """
    in_shape = tuple(int(ss) for ss in input.shape)
    if len(in_shape) == 3:
        in_shape = (1,) + in_shape
    try:
        output_shapes = [[int(s2) for s2 in sh] for sh in model.compute_output_shape(in_shape)]
    except TypeError:
        output_shapes = [[int(s2) for s2 in model.compute_output_shape(in_shape)]]
    return output_shapes

