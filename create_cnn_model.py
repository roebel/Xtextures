#! /usr/bin/env python

import os
# silence verbose TF feedback
if 'TF_CPP_MIN_LOG_LEVEL' not in os.environ:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"

import sys
placement_dir=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if placement_dir not in sys.path:
    sys.path.insert(0, placement_dir)

if __package__ is None:
    __package__ = os.path.abspath(os.path.dirname(__file__)).split("/")[-1]

import numpy as np
import json
import yaml
from argparse import ArgumentParser
import tensorflow as tf

from .as_pysrc.utils import create_logger as cl
from .as_pysrc.utils.reconstruct_command_line import reconstruct_command_line
from .XTextures_impl.XTextures_version import _version as sdtext_version
from .XTextures_impl.util import lockGPU
from .XTextures_impl.model import create_cnn_model

create_model_version = (1, 0, 0)


if __name__ == "__main__" :

    parser = ArgumentParser(description='Create CNN Model and store the model weights and config file together with preprocessing parameters for later use')

    parser.add_argument("--spec", default=os.path.join(os.path.dirname(__file__), "config", "HC08_XTspec_22.05kHz.yaml"), type=str,
                                    help='XTextures model specs file in yaml format (Def: %(default)s)')
    parser.add_argument("model_dir", default=None,
                        help="Directory for storing  keras config files and weights for further use in Xtextures.")
    parser.add_argument("-sow", "--share_overlapping_weights", action="store_true",
                        help="special weight init sharing the weights in all subnets that overlap (Def: %(default)s)")
    parser.add_argument("-p", "--plot_file", default=None,
                        help="specify a file name that will recieve a viualizaion of the mdoel  (Def: %(default)s)")
    parser.add_argument("--stft_coherent_weights", action="store_true",
                        help="initialize cnn filter weights from a valid stft (Def: %(default)s)")

    parser.add_argument('-v','--verbose', action='store_true', help='display verbose output (Def: %(default)s)')

    deprecated = parser.add_argument_group("deprecated parameters only to be used for internal models")
    deprecated.add_argument("--num_filters", default=None, type=int,
                        help=" number of filters to be used in the model, not supported by all models (Def: %(default)s)")
    deprecated.add_argument("--max_num_filters", default=None, type=int,
                    help=" maximum number of filters to be used in the model, not supported by all models (Def: %(default)s)")
    deprecated.add_argument("--activation", default="relu", choices=["elu", "leaky_relu", "relu"],
                        help="activation function (Def: %(default)s)")
    deprecated.add_argument("--alpha", default=1., type=float,
                                    help="alpha parameter for elu and leaky_relu activation if it is selected (Def: %(default)s)")

    parser.add_argument("--seed", default = 2, type = int, help="seed value for random generator initialization set to "
                                                                "negative value to not force any seed (Def: %(default)s)")

    preprocessing = parser.add_argument_group("signal STFT and preprocessing parameters")
    preprocessing.add_argument("-sr", "--sample_rate", default=22050, type=int,
                               help="sound sample rate to be enforced in the input sounds. (Def: %(default)s)")
    preprocessing.add_argument("-w", "--win_size_s", default=0.0232, type=float, help="analysis window length in seconds (Def: %(default)s)")
    preprocessing.add_argument("-ov", "--oversamp", default=2, type=float,
                        help="time domain oversamp, will be used to calc hop_size by means of win_size / oversamp (Def: %(default)s)")
    preprocessing.add_argument("-C", "--pre_scale", default = 10., type=float,
                               help="configure scaling factor for sigmoid and approx_sigmoid "
                                    "preprocessing applied to each channel of the RI spectrogram (Def: %(default)s)")
    preprocessing.add_argument("--prep_mode", default="approx_sigmoid", choices=["sigmoid", "approx_sigmoid", "soft_sqrt", "none", "time_domain",
                                                                   "si", "ap", "so", "no", "ti"],
                               help="type of normalisation to be used during optimisation, one of time_domain, "
                                    "sigmoid, approx_sigmoid, soft_sqrt, none or the first two letters of each of these. (Def: %(default)s)")


    args = parser.parse_args()
    command = reconstruct_command_line()

    default_log_format = "%(asctime)s::%(levelname)s::%(message)s"
    synth_log_format = "%(asctime)s::create_{}::%(levelname)s::%(message)s"
    ilog = cl.create_logger("Xtextures", log_format=default_log_format)
    if args.verbose:
        cl.set_stream_level(ilog, cl.logging.INFO)
    else:
        cl.set_stream_level(ilog, cl.logging.INFO, stream_select="file")

    if not os.path.exists(args.model_dir):
        os.makedirs(args.model_dir)

    cl.add_file_logger(ilog, filename=os.path.join(args.model_dir, "create_command.log"),
                       log_level = cl.logging.INFO, file_mode="w")
    cl.set_stream_message_format(ilog, log_format=synth_log_format.format(args.spec))

    ilog.info("create model create_model version {} with Xtextures implementation {}".format(create_model_version, sdtext_version))
    ilog.info(command)
    # we don't need a GPU, set all GPUs to invisible
    used_device = lockGPU(False)
    ilog.info(used_device)

    model_args = {}
    if args.num_filters is not None:
        model_args["num_filters"] = args.num_filters
    if args.max_num_filters is not None:
        model_args["max_num_filters"] = args.num_filters

    if args.seed >= 0:
        np.random.seed(args.seed)
        tf.random.set_seed(args.seed)

    preproc_dict = {'samp_rate': args.sample_rate,
                    'win_len_s': args.win_size_s,
                    'oversamp': args.oversamp,
                    'C':  args.pre_scale,
                    "prep_mode": args.prep_mode}

    with tf.device("cpu"):

        create_cnn_model(args.spec, dump_model_dir=args.model_dir, preproc_dict=preproc_dict,
                            logger=ilog, model_args=model_args, activation=args.activation,
                            alpha=args.alpha,
                            share_overlapping_weights= args.share_overlapping_weights,
                            stft_coherent_weights=args.stft_coherent_weights, plot_file=args.plot_file)

        with open(os.path.join(args.model_dir, 'sdts_config.yaml'), "w") as fo:
            yaml.dump(preproc_dict, stream=fo, default_flow_style=False, indent=4)
        ilog.info('done.')

