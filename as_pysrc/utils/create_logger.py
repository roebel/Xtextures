from __future__ import print_function

import os
import logging

def create_logger(name, env_var='PY_LOGGING_LEVEL',
                  log_level=logging.ERROR, use_stream=True, filename=None, file_mode="a",
                  log_format='%(asctime)s : %(name)s::%(levelname)s::%(message)s',
                  log_format_file = None,
                  log_date_format='%m/%d/%Y %H:%M:%S') :
    """
    returns a logging object with level configured according to env varibale PY_LOGGING_LEVEL
        with default log_level as specified via argument log_level.
        the name of the logger is specified as argument name.
    """
    
    logger = logging.getLogger(name)
    if env_var in os.environ:
        log_level=eval("logging."+os.environ[env_var].upper())
    if len( logger.handlers) == 0 :
        if use_stream :
            stream = logging.StreamHandler()
            stream.setFormatter(logging.Formatter(log_format,
                                                  datefmt=log_date_format))
            stream.setLevel(log_level)
            logger.addHandler(stream)
        if filename is not None:
            fileh = logging.FileHandler(filename, mode=file_mode, delay=True)
            if log_format_file is not None:
                fileh.setFormatter(logging.Formatter(log_format_file,
                                                        datefmt=log_date_format))
            else:
                fileh.setFormatter(logging.Formatter(log_format,
                                                        datefmt=log_date_format))
            fileh.setLevel(log_level)
            logger.addHandler(fileh)
        
    logger.setLevel(log_level)
    return logger


def add_file_logger(logger, filename,
                    log_level=logging.ERROR, file_mode="a",
                    log_format='%(asctime)s : %(name)s::%(levelname)s::%(message)s',
                    log_date_format='%m/%d/%Y %H:%M:%S'):
    """
    returns a logging object with level configured according to env varibale PY_LOGGING_LEVEL
        with default log_level as specified via argument log_level.
        the name of the logger is specified as argument name.
    """



    fileh = logging.FileHandler(filename, mode=file_mode, delay=True)
    fileh.setFormatter(logging.Formatter(log_format,
                                             datefmt=log_date_format))
    fileh.setLevel(log_level)
    logger.addHandler(fileh)

def rem_file_logger(logger, file_name):
    """
    returns a logging object with level configured according to env varibale PY_LOGGING_LEVEL
        with default log_level as specified via argument log_level.
        the name of the logger is specified as argument name.
    """

    for hh in list(logger.handlers):
        try:
            if hh.baseFilename == os.path.abspath(file_name):
                logger.remHandler(hh)
        except AttributeError :
            # not a file handler
            pass

def set_stream_level(logger, log_level=logging.ERROR, stream_select=None) :
    """
    changes all stream handlers to use given log_level
    if stream_select is given the logging level can be changed for a selected
    class of streams only. Supported selectors are

    "file" : only logging into output files will be modified 
    "std" : only logging to stdout/stderr will be modified

    Note: only the first char of the stream_select argumnet is actually used, so "f" has the same meaning as "file" and "foo"
    
    """

    if stream_select is None or logger.level > log_level :
        logger.setLevel(log_level)
        
    for hh in logger.handlers:
        if (hasattr(hh, 'stream')
            and (stream_select is None
                 or (stream_select[0] == "s" and hh.stream is not None)
                 or (stream_select[0] == "f" and hh.stream is None))):
            hh.setLevel(log_level)


def set_stream_message_format(logger, log_format='%(asctime)s : %(name)s::%(levelname)s::%(message)s',
                              log_date_format='%m/%d/%Y %H:%M:%S', stream_select=None):
    """
    changes all stream handlers to use given message format
    if stream_select is given the messge format can be changed for a selected
    class of streams only. Supported selectors are

    "file" : only logging into output files will be modified
    "std" : only logging to stdout/stderr will be modified

    Note: only the first char of the stream_select argumnet is actually used, so "f" has the same meaning as "file" and "foo"

    Parameters
    ----------
    logger: `logging.logger`
    log_format: str
    log_date_format: str
    stream_select: str
    """

    for hh in logger.handlers:
        if (hasattr(hh, 'stream')
                and (stream_select is None
                     or (stream_select[0] == "s" and hh.stream is not None)
                     or (stream_select[0] == "f" and hh.stream is None))):
            hh.setFormatter(logging.Formatter(log_format,
                                                 datefmt=log_date_format))

