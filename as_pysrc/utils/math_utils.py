from __future__ import division
import numpy as np

def nextpow2(n):
    '''
    return next power of 2 of such that 2**nextpow2(n) >= n
    '''
    return np.int32(np.ceil(np.log2(n)))

def nextpow2_val(n):
    '''
    return integer v being a power of 2 with v >= n
    '''
    v = 2
    while v < n: v = v * 2
    return v
    
def sigmoid(x, l = 1.):
    '''
    sigmoid function
    '''
    y = 1 / (1 + np.exp(-l*x))
    return y

def ratio2Cent(rat) :
    '''
    return cent representation of the ratio in rat
    '''
    return 1200.*np.log2(rat)

def cent2Ratio(cent) :
    '''
    return ratio corresponding to the cent value given as input
    '''
    return 2**(cent/1200)
