from __future__ import division, absolute_import

import os
from . import create_logger 
from . import create_logger as cl

logger = cl.create_logger("utils")
_reset_logger = False
if 'PY_LOGGING_LEVEL' in os.environ:
    eval('logger.setLevel(logging.'+os.environ['PY_LOGGING_LEVEL']+')')
else:
    cl.set_stream_level(logger, cl.logging.WARNING)
    _reset_logger = True

#import iovar as io
from .math_utils import nextpow2, nextpow2_val, cent2Ratio, ratio2Cent

