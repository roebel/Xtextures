import sys

def reconstruct_command_line(argv=None):
    """
    use list of command args to reconstruct a proper commandline including
    necessary quotes for protecting against spaces

    :param argv: list of command line arguments that normaly is fund in sys.argv

    :return: properly quoted commandline string
    """

    command=""
    for arg in argv if argv is not None else sys.argv:
        if " " in arg:
            command += "'" + arg + "'"
        else:
            command += arg
        command += " "

    return command
