# Xtextures: Cross Synthesis of Textures with 2D Convolutional Neural Network Feature Stats 

This package implements cross synthesis by means of imposition of 2D CNN Features measured on RI Spectrogam
into arbitrary sounds. The algorithm has been developed in the PhD thesis of Hugo Caracalla. 
The underlying theoretical ideas have been described [here](https://arxiv.org/pdf/1910.09497.pdf) and 
[here](https://rc.signalprocessingsociety.org/conferences/icassp/SPSICASSP20VID0987.html).


For documentation and examples please see [XTextures documentation wiki](//git.forum.ircam.fr/roebel/Xtextures/-/wikis/XTextures-Documentation)

## ChangeLog

### Xtextures v1.2.0

- added/fixed the show_stats.py script that allows vidualization of textures statistics.
- do no longer store model_config as a yaml file. TF2.6/Keras does not support the use of yaml any more.
- bug fixes.