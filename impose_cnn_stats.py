#! /usr/bin/env python

import os
# silence verbose TF feedback
if 'TF_CPP_MIN_LOG_LEVEL' not in os.environ:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"

import sys
placement_dir=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if placement_dir not in sys.path:
    sys.path.insert(0, placement_dir)

if __package__ is None:
    __package__ = os.path.abspath(os.path.dirname(__file__)).split("/")[-1]

import yaml
import json
from argparse import ArgumentParser
import tensorflow as tf

from .XTextures_impl.imposition import impose_cnn_stats
from .XTextures_impl.util import lockGPU, XtexturesLoader, finalize_job, read_snd
from .XTextures_impl.XTextures_version import _version as sdtext_version

from .as_pysrc.utils import create_logger as cl
from .as_pysrc.utils.reconstruct_command_line import reconstruct_command_line
from .as_pysrc.fileio import iovar as iov

cnn_imp_version = (1, 0, 0)


if __name__ == "__main__" :

    parser = ArgumentParser(description='run texture synthesis using RI spectrograms')

    parser.add_argument('model_dir', type=str, help='model directory containing model specs, weights, and preprocessing params ')
    parser.add_argument('synth_path', type=str, help='full path to the output file, will also be used to derive log and other file names')

    input_parser = parser.add_mutually_exclusive_group()
    input_parser.add_argument('-i', '--init_file', type=str,
                              help='filename of init file to be used as sources for receiving'
                                 ' target stats, if not set or set none, random init files will be created using either the lengths '
                                   'of the target files or the length provided by means of sig_len_s flag')
    input_parser.add_argument("--sig_len_s", default=None, type = float,
                           help="len of synth signals in seconds, will be equal to the length of the input file, "
                                 "if that is provided (Def: %(default)s)")


    out_specs = parser.add_argument_group("output_specs", description="spec related to output sound")
    out_specs.add_argument("-f", "--format", default="wav", help = "file format for synthesized files (Def: %(default)s)")
    out_specs.add_argument("--enc", default="float32", help = "snd encoding for synthesized files (can be changed only "
                                                              "when pysndfile is avilable) (Def: %(default)s)")
    out_specs.add_argument("--ored", default=0., type=float, help="if ored > 0 then intermediate output files are stored "
                                                                  "whenever the loss has decreased by more than the given factor (Def: %(default)s)")
    out_specs.add_argument("--osteps", default=None, type=int,
                           help="intermediate output files are stored "
                                "whenever the at most this number of steps have been performed after the last file that was stored (Def: %(default)s)")

    processing = parser.add_argument_group("processing options", description="options modifying imposition and logging")
    processing.add_argument("--target_stats_files", default=[], nargs="+",
                        help="files containing GRAM target matrices. These files need to be compatible with the model provided in model_dir."
                             "This means they should have been created using dump_cnn_stats.py using the same model (Def: )")
    processing.add_argument('--ffmpeg', default='ffmpeg', help='ffmpeg executable in case you want to read mp3 files (Def: %(default)s)')
    processing.add_argument('--cpu', action='store_true', help='force running on cpu even if GPU is available')
    processing.add_argument('-v','--verbose', action='store_true', help='display verbose output (Def: %(default)s)')
    processing.add_argument('-l','--log', action='store_true', help='create tensorboard log directory (Def: %(default)s)')
    processing.add_argument('-p','--progress', default=5, type=int, help='display and store progress every given number of steps  (Def: %(default)s)')
    processing.add_argument("-n", "--num_iter", default=100, type = int, help="number of optimizer iterations (Def: %(default)s)")
    processing.add_argument("--norm_mode", default="nusty", choices=["usty", "gatys", "nusty", "none"],
                        help="type of normalisation to be used during optimisation, one of usty, gatys, nusty, none. "
                             "only nusty supports cross synthesis between sounds of different length (Def: %(default)s)")
    processing.add_argument("--num_threads", default=1, type=int,
                        help="number of CPU threads to be used by tensorflow. (Def: %(default)s)")

    have_debug = False
    if "XTEXTURES_DEBUG" in os.environ:
        have_debug = True
        debug = parser.add_argument_group("parameters used for debugging only", description="stay away - this may not work!")
        debug.add_argument('--optimizer', default = "l-bfgs-b", type=str,
                            choices=["l-bfgs-b", "adam"], help='optimizer to be used. One of l-bfgs-b and adam (Def: %(default)s)')
        debug.add_argument('--lr', default = 0.001, type=float,
                            help='learning rate of optimizer in case adam is used as optimizer (Def: %(default)s)')
        debug.add_argument("--debug_placement", action="store_true",
                            help="log device placement  (Def: %(default)s)")
        debug.add_argument('--gather_run_metadata', action='store_true', help='gather run_metadata for profiling and memory usage directory (Def: %(default)s)')


    args = parser.parse_args()
    command = reconstruct_command_line()
    if have_debug and args.debug_placement:
        tf.debugging.set_log_device_placement(True)

    tf.config.threading.set_intra_op_parallelism_threads(args.num_threads)
    tf.config.threading.set_inter_op_parallelism_threads(args.num_threads)

    default_log_format = "%(asctime)s::%(levelname)s::%(message)s"
    synth_log_format = "%(asctime)s::{}::%(levelname)s::%(message)s"
    ilog = cl.create_logger("sdtext", log_format=default_log_format)
    if args.verbose:
        cl.set_stream_level(ilog, cl.logging.INFO)
    else:
        cl.set_stream_level(ilog, cl.logging.INFO, stream_select="file")

    out_file_name = args.synth_path
    synth_path = os.path.dirname(out_file_name)
    output_name = os.path.splitext(os.path.basename(out_file_name))[0]
    out_log_file = os.path.join(synth_path, output_name + ".log")
    cl.add_file_logger(ilog, filename=out_log_file, log_level=cl.logging.INFO, file_mode="w")
    cl.set_stream_message_format(ilog, log_format=synth_log_format.format(output_name))

    if os.path.exists(os.path.join(args.model_dir, "sdts_config.yaml")):
        with open(os.path.join(args.model_dir, "sdts_config.yaml"), "r") as fi:
            preproc_dict = yaml.load(stream=fi, Loader=XtexturesLoader)
    elif os.path.exists(os.path.join(args.model_dir, "sdts_config.json")):
        with open(os.path.join(args.model_dir, "sdts_config.json"), "r") as fi:
            preproc_dict = json.load(fp=fi)
    else:
        raise RuntimeError(f"none of the supported preprocessing config files: sdts_config.yaml and sdts_config.json, found in {args.model_dir}!")

    os.makedirs(synth_path, exist_ok=True)
    with open(os.path.join(synth_path, 'sdts_config.yaml'), "w") as fo:
        yaml.dump(preproc_dict, stream=fo, default_flow_style=False, indent=4)

    log_dir = None
    init_sig = None
    samp_rate = None
    syn_sig_len = None

    gram_stat = iov.load_var(args.target_stats_files[0])
    if samp_rate in gram_stat:
        samp_rate = gram_stat["samp_rate"]
    if samp_rate:
        if preproc_dict["samp_rate"] != samp_rate:
            raise RuntimeError("impose_cnn_stats:error:: sample rate in stats file and sample rate in snd do not match!")
    else:
        samp_rate = preproc_dict['samp_rate']

    if args.init_file:
        init_sig = read_snd(args.init_file, samp_rate, logger=ilog, ffmpeg_bin=args.ffmpeg)
        syn_sig_len = init_sig.size
    elif args.sig_len_s is not None:
        if not samp_rate:
            raise RuntimeError("impose_cnn_stats:error:: you use old stats files not containing sample rate in formtion please recaclulate the stats files!")
        syn_sig_len = int(preproc_dict['samp_rate'] * args.sig_len_s)

    else:
        if "snd_len" in gram_stat:
            syn_sig_len = gram_stat["snd_len"]
        else:
            raise  RuntimeError("impose_cnn_stats.py::error::cannot determine snd lengths please provide sg_len_s option")


    ilog.info("running cnn imposer version {} with Xtextures implementation {}". format(cnn_imp_version, sdtext_version))

    used_device = lockGPU(not args.cpu, ilog)
    ilog.info(used_device)
    norm = args.norm_mode
    num_iter = args.num_iter

    with tf.device(used_device):

        if args.log or (have_debug and args.gather_run_metadata):
            log_dir = os.path.join(synth_path, output_name + "_logs")

        outfile_pat = None
        if args.ored > 0:
            odir = os.path.join(synth_path, output_name+"_snds")
            if os.path.exists(odir):
                for file in os.listdir( odir ):
                    path = os.path.join(odir, file)
                    if os.path.isfile(path):
                        os.remove(path)
            else:
                os.makedirs(os.path.join(synth_path, output_name+"_snds"),exist_ok=True)
            outfile_pat = os.path.join(odir, output_name+"_{0:>05d}_{1:>0.5f}."+args.format)

        ilog.info(f"start synthesis of {output_name} from {args.target_stats_files}")
        ilog.info(f"command line :<{command}>")
        ret = impose_cnn_stats(model_dir=args.model_dir,
                               target_stats_files=args.target_stats_files,
                               init_data=init_sig, samp_rate=samp_rate, norm=norm, weights=None,
                               num_iter=num_iter, preproc_dict=preproc_dict,
                               log_dir=log_dir,
                               gather_run_metadata=args.gather_run_metadata if have_debug else False,
                               syn_sig_len=syn_sig_len, progress=args.progress,
                               logger=ilog,
                               opt=args.optimizer if have_debug else "l-bfgs-b",
                               adam_learning_rate=args.lr if have_debug else 0.001,
                               outfile_pat=outfile_pat,
                               outfile_red=args.ored,
                               outfile_steps=args.osteps,
                               commandline=command)

        finalize_job(ret, synth_path=synth_path, output_name=output_name, args=args, preproc_dict=preproc_dict,
                     ilog=ilog, command=command, out_log_file=out_log_file,
                     default_log_format=default_log_format,
                     count=1, synth_num=1, rem_file_logger=False)

        ilog.info('done.')

