#! /bin/bash



usage() {
    echo "Usage: $0  [-h|--help]  [-t|--mod_tag Def:TF01]  [-i|--inp input audio sound ] [-s|--stats] [-o|--out_dir] [-n|--num]" 1>&2;
    echo "n model dir for the given snd " 1>&2;
    echo "-t|--mod_tag : path to model dir (Def: TF01)" 1>&2;
    echo "-i|--inp     : path to snd " 1>&2
    echo "-o|--outfile : output fil, this wil; be ued to deirve the output directroy where other files wil be wrritten as well " 1>&2
    echo "-n|--num     : number of iterations " 1>&2
    echo "-s|--stats   : list of stats files to impose as replacement of the sounds own stats" 1>&2
    echo "-d|--dry run : display the comands to be run, don't run anything" 1>&2
    exit 1;
}


SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

# set -x
cd "${REPOSDIR}"

runner=
NIT=2000
MODTAG=TF01
INSOUND=
outfile=
debug=0
stats_list=()
while [ "$1" != "" ]; do
    case "$1" in
        -i | --inp* )
            shift
            INSOUND="$1"    
            shift
            ;;
        -o | --out* )
            shift
            outfile="$1"    
            shift
            ;;
        -s | --stats )
            shift;
            while [ "${1}" != "" ]; do
                if [ "${1:0:1}" != "-" ]; then
                    echo "add ${1} as a input stat"
                    stats_list+=( ${1} )
                    shift
                else
                    break
                fi
            done
            ;;    
        -h)
            usage
            shift
            ;;
        -n | --num*)
            shift
            NIT=$1
            shift
            ;;
        -d | --dry*)
            runner=echo
            shift
            ;;
        --debug)
            debug=1
            shift
            ;;
        *)
            echo "unknown option: $1"
            usage
            shift
            ;;
    esac
done

INPUT=
if [ "$INSOUND" != "" ];then
    INPUT="--init=$INSOUND"
fi
if [ "$outfile" = "" ];then
    echo "usage: $0::error::no output file selected, select with -o|--out"
    exit 1
fi
if [ "${#stats_list[@]}" = "0" ];then
    echo "usage: $0::error:: no stats for replacement selected"
    exit 1
fi

if [ "$debug" = "1" ]; then
    set -x
fi
MODTAG=
for stat in "${stats_list[@]}"; do
    MODCAND=$(echo  $(dirname $stat) | sed -e "s+.*_\([^_]*\)/stats/.*+\1+g")
    if [ "$MODTAG" = "" ]; then
        MODTAG=$MODCAND
    fi
    if [ "$MODTAG" != "$MODCAND" ]; then
        echo "$0::error:: you cannot mix stats files from different models ${MODTAG} ${MODCAND}"
        exit 1
    fi
done

MODELDIR="./tmp/cnn_model_${MODTAG}"

if [ "$runner" = "" ]; then    
    echo ./impose_cnn_stats.py "${MODELDIR}" "${outfile}" -n $NIT ${INPUT} --target_stats_files  "${stats_list[@]}"  --verbose
fi
$runner ./impose_cnn_stats.py "${MODELDIR}" "${outfile}" -n $NIT ${INPUT} --target_stats_files  "${stats_list[@]}"  --verbose
