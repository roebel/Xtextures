#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)
cd "${REPOSDIR}"

usage() {
    echo "Usage: $0 [-h|--help]  [-t|--mod_tag]  [-s|--snds] [-o|--order_first T/F]" 1>&2;
    echo "create model for the given tag add create stats for all snd provided after --snds " 1>&2;
    echo "-t|--mod_tag: only synchronize synthesized sound files " 1>&2;
    echo "-d|--dry run : display the commands to be run, don't run anything" 1>&2
    echo "-h|--help   : display this help " 1>&2;
    exit 1;
}


runner=
DRY=
#set -x
set -e

outdir=

MTAG=TF01
while [ "$1" != "" ]; do
    case "$1" in
        -t | --mtag* )
            shift
            MTAG="$1"    
            shift
            ;; 
        -o | --outdir )
            shift
            outdir="$1"    
            shift
            ;;
        -h)
            usage
            shift
            ;;
        -d | --dry*)
            runner=echo
            shift
            ;;
         *)
            echo "$0::error::unknown flag $1"
            exit 1
            ;;
    esac
done

if [ "$outdir" = "" ];then
    echo "usage: $0::error::no output directory selected, select with -o|--outdir"
    exit 1
fi


$runner ./example_scripts/generate_model_and_stats.sh -t ${MTAG} -s  $(cat ./snd_examples/snd_list.txt)
MODELDIR="./tmp/cnn_model_${MTAG}"

allstats=( $(./example_scripts/get_all_stats.sh --order S ./config/${MTAG}_XTspec_22.05kHz.yaml ) )

for ff in $(cat ./snd_examples/snd_list.txt); do
    echo ==========================
    BASEOUT=$(basename $ff .flac)
    statdd=${MODELDIR}/stats/${BASEOUT}
    # We iterate in incremental manner over the statistics to produce a more and more complete texture
    stat_list=( "$statdd/input.std.p"  )
    name=${MTAG}_
    for stat_base in "${allstats[@]}" ; do
        stat_list+=( "$statdd/Conv2D_${stat_base}.GRAM.p" )
        name=${name}+${stat_base}
        out_file="${outdir}/${BASEOUT}_${name}/${BASEOUT}_${name}.wav"
        $runner ./impose_cnn_stats.py "${MODELDIR}" "${out_file}" -n 2000   $init --target_stats_files  ${stat_list[@]}  --verbose
        echo --------------------------
        out_file="${outdir}/${BASEOUT}_${name}/${BASEOUT}_${name}_20s.wav"
        $runner ./impose_cnn_stats.py "${MODELDIR}" "${out_file}" -n 2000 --sig_len 20  $init --target_stats_files  ${stat_list[@]}  --verbose
    done
done

