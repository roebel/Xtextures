#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

cd "${REPOSDIR}"
BASEINW=82058_C1_77-87_benboncan__lapping-waves

init=
OT=
if [ "$1" = "-i" ]; then
  init="--init=./snd_examples/104956_13.8-23.8_little_fire_3.wav"
  OT="_FromF"
fi


BASEOUTW=waves
BASESHORT=W

MODELDIR="./tmp/cnn_model"
outdir=./tmp/resynth_${BASEOUTW}${OT}
statdd="${MODELDIR}/stats/${BASEINW}"

./example_scripts/generate_model.sh "${MODELDIR}"
# produce stat directory if it does not exist already
./example_scripts/generate_stats.sh "${MODELDIR}" "${BASEINW}"

set -x

allstats=( $(grep name config/TF01_XTspec_22.05kHz.yaml | grep Conv2D | cut -d":" -f2 | tr -d " " | sort -n -t_ -k2 -k3 | cut -d_ -f2- ) )
# We iterate in incremental manner over the statistics to produce a more and more complte texture
stat_list=( "$statdd/input.std.p"  )
name=
for stat_base in  ${allstats[@]}; do
    stat_list+=( "$statdd/Conv2D_${stat_base}.GRAM.p" )
    name=${name}+${stat_base}
    out_file="${outdir}/${BASEOUTW}_${name}/${BASESHORT}_${name}.wav"
    ./impose_cnn_stats.py "${MODELDIR}" "${out_file}" -n 2000   $init --target_stats_files  ${stat_list[@]}  --verbose
done
