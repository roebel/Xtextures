#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)
cd "${REPOSDIR}"

usage() {
    echo "Usage: $0  [-h|--help]  [-m|--mod_dir] [-s|--snd] " 1>&2;
    echo "create stats in model dir for the given snd " 1>&2;
    echo "-m|--mod_dir : path to model dir " 1>&2;
    echo "-s|--snd     : path to snd " 1>&2;
    echo "-d dry run " 1>&2;
    exit 1;
}


while [ "$1" != "" ]; do
    case "$1" in
        -m | --mod_dir )
            shift
            MODELDIR="$1"    
            shift
            ;;
        -s | --snd )
            shift
            SNDIN="$1"
            BASEIN=$(basename ${SNDIN%.*}) 
            shift
            ;;    
        -h)
            usage
            shift
            ;;
        *)
            echo "unknown option: $1"
            usage
            shift
            ;;
    esac
done


if [ "$MODELDIR" = "" ]; then
    echo "$0:error:: no model dir is given please select with option -m or --mod_dir"
    exit 1
fi


# do not recreate the model once it exists
if [ ! -f "${MODELDIR}/model_config.json" ]; then
  echo "${MODELDIR}  does not exist. Please create the CNN model first"
  exit 1
fi

set -e
#set -x
statdd="${MODELDIR}/stats/${BASEIN}"

allstats=( $(grep name:  "${MODELDIR}/model_config.yaml" | grep Conv2D_ | cut -d":" -f2 | tr -d " " | sort -n -t_ -k2 -k3 | cut -d_ -f2- |uniq ) )

# produce stat directory if it does not exist already or if content is not complete
done=0
for stat_base in "${allstats[@]}"; do
   if [ ! -f "$statdd/Conv2D_${stat_base}.GRAM.p" ]; then
       echo ./dump_cnn_stats.py "${MODELDIR}" -t ./snd_examples/${BASEIN}.*
       ./dump_cnn_stats.py "${MODELDIR}" -t ./snd_examples/${BASEIN}.*
       done=1
       break
   fi
done

if [ "$done" = "0" ];then
    echo stats for  ./snd_examples/${BASEIN}.* exist in ${statdd}
fi

