#! /bin/bash


SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

cd "${REPOSDIR}"

usage() {
    echo "Usage: $0 [-h|--help]  [-t|--mod_tag]  [-s|--snds] [-o|--order_first T/F]" 1>&2;
    echo "create model for the given tag add create stats for all snd provided after --snds " 1>&2;
    echo "-t|--mod_tag: only synchronize synthesized sound files " 1>&2;
    echo "-o|--order_first [T, F or S] : stat tags can be orderer in three ways. If T is given then ordering will be with respect to increasing time length" 1>&2;
    echo "                            if F is selected then frequency band width will be the major ordering criterion. Finally for S it will be the total size of the filter patch" 1>&2;
    echo "-s|--snds   : one or multple sound files " 1>&2;
    echo "-h|--help   : display this help " 1>&2;
    exit 1;
}


sortmode=S
list=()
while [ "$1" != "" ]; do
    case "$1" in
        -m | --mod* )
            shift
            model_dir="$1"    
            shift
            ;; 
        -c | --config )
            shift
            configfile="$1"    
            shift
            ;;
        -o | --order* )
            shift
            sortmode="$1"
            if [ "$sortmode" = "T"  ]; then
                :
            else
                if [ "$1" = "F"  ]; then
                    :
                else
                    if [ "$1" = "S"  ]; then
                        :
                    else
                        echo "invalid sort mode selected, please select either T, F or S"
                        exit 1
                    fi
                fi
            fi
            shift
            ;;
        -s | --stats )
            shift
            statsdir="$1"    
            shift
            ;;
        -h)
            usage
            shift
            ;;
        *)
            list+=( "$1" )
            shift
            ;;
    esac
done

if [ "${#list[@]}" = "0" ] ; then
    echo no input selected, select either a model dir, Xtextures config file or a stats directory
    exit 1
fi

function get_size_labels {
    ll="$1"
    if [ -f "$ll" ]; then
        if grep -q "output_layers:" "$ll" ; then
            grep --regexp="- - Conv2D_"  "${ll}" | tr -d " " |  cut -d_ -f2- | uniq 
        else
            grep Conv2D_  "${ll}" | tr -d " " | cut -d":" -f2 | cut -d_ -f2- | uniq 
        fi
    else       
        if [ -f "${ll}/model_config.yaml" ]; then
            grep --regexp="- - Conv2D_"  "${ll}/model_config.yaml" | tr -d " " |  cut -d_ -f2- | uniq 
        else
            ls -1 ${ll} | grep Conv2D_   |  cut -d"." -f1 | cut -d_ -f2-
        fi
    fi
}

function sort_labels () {

   if [ $sortmode = T ]; then
       for cc in $(get_size_labels "$1"); do
           echo $cc
       done |  sort -n -t _ -k2 -k1
   fi
   if [ $sortmode = F ]; then
       for cc in $(get_size_labels "$1"); do
           echo $cc
       done |  sort -n -t _ -k1 -k2
   fi
   if [ $sortmode = S ]; then
       for cc in $(get_size_labels "$1"); do
           prod=$( echo $cc | tr "_" "*" | bc -l )
           echo ${prod}_$cc
       done |  sort -n -t _ -k1 -k3 -k2 | cut -d_ -f2-
   fi
}


#set -x

for ll in "${list[@]}"; do
    sort_labels $ll 
done



