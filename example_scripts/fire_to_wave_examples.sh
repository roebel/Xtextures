#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

cd "${REPOSDIR}"
BASEINW=82058_C1_77-87_benboncan__lapping-waves
BASEINF=104956_13.8-23.8_little_fire_3
BASEOUTF=fire
BASEOUTW=waves
BASEOUTFW=FtoW
BASESHORT=FtoW
MODELDIR="./tmp/cnn_model"
outdir=./tmp/resynth_${BASEOUTFW}
statFdd="${MODELDIR}/stats/${BASEINF}"
statWdd="${MODELDIR}/stats/${BASEINW}"

./example_scripts/generate_model.sh "${MODELDIR}"
# produce stat directory if it does not exist already
./example_scripts/generate_stats.sh "${MODELDIR}" "${BASEINF}"
./example_scripts/generate_stats.sh "${MODELDIR}" "${BASEINW}"

allstats=( $(grep name config/TF01_XTspec_22.05kHz.yaml | grep Conv2D | cut -d":" -f2 | tr -d " " | sort -n -t_ -k2 -k3 | cut -d_ -f2- ) )
# We iterate in incremental manner over the statistics to produce a more and more complte texture
statW_list=( "$statWdd/input.std.p"  )
statF_list=( "$statFdd/input.std.p"  )

set -x

# construct the full set of stats for both sounds
for stat_base in  ${allstats[@]}; do
    statW_list+=( "$statWdd/Conv2D_${stat_base}.GRAM.p" )
    statF_list+=( "$statFdd/Conv2D_${stat_base}.GRAM.p" )
done


name=
for nlen in $( seq 1 ${#statW_list[@]} ); do
    # replace an increasing number of local statistics by the waves

    w_len=$(( ${nlen} + 1 ))
    stat_list=( ${statW_list[@]:0:${w_len}} )
    name=W
    for kk in  ${allstats[@]:0:${nlen}}; do
        name=${name}"+${kk}"
    done
    # fill the rest from the fire
    stat_list+=( ${statF_list[@]:${w_len}} )
    name=${name}"_F"
    for kk in  ${allstats[@]:${nlen}}; do
        name=${name}"+${kk}"
    done
    out_file="${outdir}/${BASEOUTFW}_${name}/${BASESHORT}_${name}.wav"
    ./impose_cnn_stats.py "${MODELDIR}" "${out_file}" -n 2000 --target_stats_files  ${stat_list[@]} --ored 0.7 --verbose
done
