#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

cd "${REPOSDIR}"

usage() {
    echo "Usage: $0 [-h|--help]  [-t|--mod_tag]  [-s|--snds] " 1>&2;
    echo "create model for the given tag ad create stats for all snd provided after --snds " 1>&2;
    echo "-t|--mod_tag: only synchronize synthesized sound files " 1>&2;
    echo "-s|--snds   : one or multple sound files " 1>&2;
    echo "-h|--help   : display this help " 1>&2;
    exit 1;
}


MODTAG=TF01
INSOUND=
outdir=
snds_list=()
while [ "$1" != "" ]; do
    case "$1" in
        -t | --mod_tag )
            shift
            MODTAG="$1"    
            shift
            ;;
        -s | --snds )
            shift;
            while [ "${1}" != "" ]; do
                if [ "${1:0:1}" != "-" ]; then
                    echo "add ${1} as a input snd"
                    snds_list+=( ${1} )
                    shift
                else
                    break
                fi
            done
            ;;    
        -h)
            usage
            shift
            ;;
        *)
            echo "unknown option: $1"
            usage
            shift
            ;;
    esac
done

if [ "${#snds_list[@]}" = "0" ]; then
    echo "no sound files given"
    echo "use $0 -h for help"
    exit
fi

set -e


MODELDIR="./tmp/cnn_model_${MODTAG}"

#set -x

./example_scripts/generate_model.sh "${MODELDIR}" ${MODTAG}

for snd in "${snds_list[@]}"; do
    ./example_scripts/generate_stats.sh -m "${MODELDIR}" -s "$snd"
done


