#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)
cd "${REPOSDIR}"


SMALLSHAPE=( 3_3 5_5 )
MTAG=LS01
LARGESHAPE=( 48_3 105_3 2_1000 3_108 )
MTAG=LS02
LARGESHAPE=( 5_11 5_23 5_56 5_157 11_5 47_5 101_3 )

function getpat_small () {
    for ll in ${SMALLSHAPE[@]}; do
	echo "$1/Conv2D_${ll}.GRAM.p";
    done;
}

function getpat_large () {
    for ll in ${LARGESHAPE[@]}; do
	echo "$1/Conv2D_${ll}.GRAM.p";
    done;
}

DRY=
#set -x
set -e

./example_scripts/generate_model_and_stats.sh -t ${MTAG} -s  $(cat ./snd_examples/snd_list.txt)

for fs in $(cat ./snd_examples/snd_list.txt); do
    echo ==========================
    SMBASE=$(basename ${fs} .flac)
    SMTAG=$(echo ${SMBASE%%J*} | cut -d_ -f2)
    for fl in $(cat ./snd_examples/snd_list.txt); do
        echo --------------------------
        LGBASE=$(basename ${fl} .flac)
        LGTAG=$(echo ${LGBASE%%J*} | cut -d_ -f2)
        if [ "$LGTAG" != "$SMTAG" ]; then
            ./example_scripts/replace_stats.sh $DRY -s $(getpat_small tmp/cnn_model_${MTAG}/stats/$SMBASE) $(getpat_large tmp/cnn_model_${MTAG}/stats/$LGBASE) -o tmp/replace_${MTAG}/${SMTAG}_S_${LGTAG}_L.flac
        fi
    done

done

