#! /bin/bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"
REPOSDIR=$(dirname $SCRIPTDIR)

# set -x
cd "${REPOSDIR}"

if [ "$1" = "" ];then
    echo "usage: $0 model_output_dir config_file[TF01/TF02]"
    exit 1
fi
if [ "$2" = "" ];then
    echo "usage: $0 model_output_dir config_file[TF01/TF02]"
    exit 1
fi

MODELDIR="$1"
CONFIGSPEC="$2"_XTspec_22.05kHz.yaml


# do not recreate the model once it exists
if [ ! -f "${MODELDIR}/model_config.json" ]; then
    echo ./create_cnn_model.py "${MODELDIR}" --sample_rate=22050 --win_size_s=0.0232 --seed 1 --spec ./config/$CONFIGSPEC
    ./create_cnn_model.py "${MODELDIR}" --sample_rate=22050 --win_size_s=0.0232 --seed 1 --spec ./config/$CONFIGSPEC
fi
