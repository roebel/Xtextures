The files in this folder are provided under the following licences:

- **104956_18.8-23.8_little_fire_3.flac**: [Creative Commons Attribution Licence](https://creativecommons.org/licenses/by/3.0/)

  This file has been generated from [freesound.org::104956](https://freesound.org/s/104956/)
  by means of downsampling to 22050 Hz and extracting the segment from 13.8-23.8s.
   
- **82058_C1_77-87_benboncan__lapping-waves.flac**: [Creative Commons Attribution Licence](https://creativecommons.org/licenses/by/3.0/)
  
  This file has been generated from [freesound.org::82058](https://freesound.org/s/82058/)
  by means of selecting the first channel, downsampling to 22050Hz and extracting the segment from 77-87s.

- **MusicDelta_FunkJazz_Drum_35.4-44.1.flac**:  [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

  This file has been generated from  the file MedleyDB/Audio/MusicDelta_FunkJazz/MusicDelta_FunkJazz_RAW/MusicDelta_FunkJazz_RAW_01_01.wav  of MedleyDB version 1. The original mono file has been downsampled to 22050Hz, converted to flac format and cropped
  to the segment 35.4-44.1s.

- **MusicDelta_FreeJazz_Drum_49.8-57.9.flac**:  [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

  This file has been generated from  the file MedleyDB/Audio/MusicDelta_FreeJazz/MusicDelta_FreeJazz_RAW/MusicDelta_FreeJazz_RAW_01_01.wav  of MedleyDB version 1. The original mono file has been downsampled to 22050Hz, converted to flac format and cropped  to the segment 49.8-57.9s.

- **MusicDelta_FunkJazz_Drum_35.4-44.1.flac**:  [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

  This file has been generated from  the file MedleyDB/Audio/MusicDelta_FunkJazz/MusicDelta_FunkJazz_RAW/MusicDelta_FunkJazz_RAW_01_01.wav  of MedleyDB version 1. The original mono file has been downsampled to 22050Hz, converted to flac format and cropped
  to the segment 35.4-44.1s.

- **MusicDelta_FusionJazz_Drum_95.5-106.0.flac**:  [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

  This file has been generated from  the file MedleyDB/Audio/MusicDelta_FusionJazz/MusicDelta_FusionJazz_RAW/MusicDelta_FusionJazz_RAW_01_01.wav  of MedleyDB version 1. The original mono file has been downsampled to 22050Hz, converted to flac format and cropped
  to the segment 95.5-106.0s.
